#
# Copyright 2021-2023 Anthony Paul Astolfi
#
cmake_minimum_required(VERSION 3.16)
project(batteries CXX)

# Enable link-time optimization. NOTE: currently breaks arm64 Linux (M1)
#
set(CMAKE_INTERPROCEDURAL_OPTIMIZATION FALSE)

# The src dir should appear in the include path.
#
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)

# Generate compile_commands.json.
#
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Enable ccache on systems where it is installed.
#
find_program(CCACHE_PROGRAM ccache)
if (CCACHE_PROGRAM)
  set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
  message("Enabled ccache builds")
endif ()

message("BATT_BUILD_TESTS=$ENV{BATT_BUILD_TESTS}")

#==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
# Include the options file we generated in conanfile.py.
#
include(${CMAKE_BINARY_DIR}/batteries_options.cmake)
include(${CMAKE_BINARY_DIR}/conan_find_requirements.cmake)

message("")
message("BATTERIES_OPTION_HEADER_ONLY is ${BATTERIES_OPTION_HEADER_ONLY}")
message("BATTERIES_OPTION_WITH_GLOG is ${BATTERIES_OPTION_WITH_GLOG}")
message("BATTERIES_OPTION_WITH_PROTOBUF is ${BATTERIES_OPTION_WITH_PROTOBUF}")
message("BATTERIES_OPTION_WITH_ASSERTS is ${BATTERIES_OPTION_WITH_ASSERTS}")
message("")

#==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
# General compile options setup.
#
if (WIN32)
    add_compile_options(/Zc:__cplusplus)
    add_compile_definitions(WIN32_LEAN_AND_MEAN)
    add_compile_definitions(_ENABLE_EXTENDED_ALIGNED_STORAGE)
else ()
    add_compile_options(-Wall -Wextra -Werror)
    add_compile_options(-Werror=switch-enum -Wswitch-enum)
    add_compile_options(-Wsuggest-override -Woverloaded-virtual)
    add_compile_options(-fno-diagnostics-color)
    add_compile_options(-ftemplate-backtrace-limit=0)
endif ()

add_compile_definitions(BOOST_ERROR_CODE_HEADER_ONLY)

file(GLOB TestSources
     batteries/*_test.cpp
     batteries/*/*_test.cpp
     batteries/*/*/*_test.cpp
     batteries/*.test.cpp
     batteries/*/*.test.cpp
     batteries/*/*/*.test.cpp)

#==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
# Calculate the list of library dependencies
#
set(LibraryDeps pcg-cpp::pcg-cpp Boost::context)
if (WIN32)
    list(APPEND LibraryDeps Boost::stacktrace)
else ()
    list(APPEND LibraryDeps Boost::stacktrace_backtrace)
endif ()

if (libunwind_FOUND)
  list(APPEND LibraryDeps libunwind::unwind)
endif ()

if ("${BATTERIES_OPTION_WITH_GLOG}" STREQUAL "True")
  list(APPEND LibraryDeps glog::glog)
endif ()

if ("${BATTERIES_OPTION_WITH_PROTOBUF}" STREQUAL "True")
  list(APPEND LibraryDeps protobuf::libprotobuf)
endif ()

if (WIN32)
else ()
  list(APPEND LibraryDeps dl)
endif ()

message("LibraryDeps is ${LibraryDeps}")

#==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
# Create `batteries` target, either as interface or actual library
#
if ("${BATTERIES_OPTION_HEADER_ONLY}" STREQUAL "True")

  add_library(batteries INTERFACE)

  foreach (Dep IN ITEMS ${LibraryDeps})
    target_link_libraries(batteries INTERFACE ${Dep})
  endforeach ()

else ()

  file(GLOB Sources
    batteries/*.cpp
    batteries/*/*.cpp
    batteries/*/*/*.cpp)

  foreach (_file "FORCE_LIST_NOT_EMPTY;${TestSources}")
    list(REMOVE_ITEM Sources ${_file})
  endforeach ()

  add_library(batteries ${Sources})

  foreach (Dep IN ITEMS ${LibraryDeps})
    target_link_libraries(batteries PUBLIC ${Dep})
  endforeach ()

  # Default packaging instructions for the library.
  #
  install(TARGETS batteries)

endif ()

message("Sources is ${Sources}")
message("TestSources is ${TestSources}")

#==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
# Define unit test (BattTest) target.

if ("$ENV{BATT_BUILD_TESTS}" STREQUAL "1")

  add_executable(BattTest ${TestSources})

  include_directories(${GTest_INCLUDE_DIRS})

  target_link_libraries(BattTest PRIVATE
    GTest::gtest
    GTest::gtest_main
    GTest::gmock
    GTest::gmock_main
    batteries
    )

endif ()
