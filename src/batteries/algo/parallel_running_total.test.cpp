//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#include <batteries/algo/parallel_running_total.hpp>
//
#include <batteries/algo/parallel_running_total.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>
#include <random>

namespace {

using namespace batt::int_types;

using batt::TaskCount;
using batt::TaskSize;
using batt::WorkerPool;
using batt::WorkSliceParams;

using batt::parallel_running_total;
using batt::RunningTotal;

constexpr bool kVerbose = false;

struct CustomValue {
    int x = 0;
    int y = 0;
    int z = 0;

    CustomValue& operator+=(const CustomValue& r)
    {
        this->x += r.x;
        this->y += r.y;
        this->z += r.z;
        return *this;
    }
};

CustomValue operator+(const CustomValue& l, const CustomValue& r)
{
    return CustomValue{
        .x = l.x + r.x,
        .y = l.y + r.y,
        .z = l.z + r.z,
    };
}

bool operator==(const CustomValue& l, const CustomValue& r)
{
    return l.x == r.x && l.y == r.y && l.z == r.z;
}

inline std::ostream& operator<<(std::ostream& out, const CustomValue& t)
{
    return out << "{.x=" << t.x << ", .y=" << t.y << ", .z=" << t.z << ",}";
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AlgoParallelRunningTotalTest, Randomized)
{
    WorkerPool& worker_pool = WorkerPool::default_pool();

    for (usize seed = 0; seed < 100; ++seed) {
        if (kVerbose) {
            std::cerr << BATT_INSPECT(seed) << std::endl;
        }
        usize seed_delta = 0;
        for (usize input_size : {0, 1, 10, 100, 10 * 1000}) {
            for (usize min_delta : {0, 1, 10, 100}) {
                for (usize max_delta : {min_delta * 1, min_delta * 2, min_delta * 10}) {
                    std::default_random_engine rng{(u32)(seed + seed_delta)};
                    std::uniform_int_distribution<usize> pick_num{min_delta, max_delta};

                    std::vector<usize> input;
                    std::vector<usize> expected_output;
                    {
                        usize total = 0;
                        expected_output.emplace_back(0);
                        for (usize n = 0; n < input_size; ++n) {
                            input.emplace_back(pick_num(rng));
                            total += input.back();
                            expected_output.emplace_back(total);
                        }
                    }

                    if (kVerbose) {
                        std::cerr << std::endl
                                  << "input=    " << batt::dump_range(input) << std::endl
                                  << "expected= " << batt::dump_range(expected_output) << std::endl;
                    }

                    for (usize min_task_size : {1, 3, 11, 199, 10 * 1000}) {
                        if (min_task_size > input_size) {
                            continue;
                        }
                        for (usize max_tasks : {1, 7, 8, 13, 217}) {
                            if (max_tasks > input_size) {
                                continue;
                            }

                            RunningTotal actual_output = batt::parallel_running_total(
                                worker_pool, input.begin(), input.end(),
                                WorkSliceParams{/*min_task_size=*/TaskSize{min_task_size},
                                                /*max_tasks=*/TaskCount{max_tasks}});

                            if (kVerbose) {
                                std::cerr << actual_output.dump() << std::endl;
                            }

                            EXPECT_EQ(actual_output.size(), input_size + 1);
                            EXPECT_EQ(actual_output.empty(), input_size + 1 == 0);
                            EXPECT_FALSE(actual_output.empty());
                            EXPECT_EQ(actual_output.front(), 0u);
                            if (input_size > 0) {
                                EXPECT_EQ(actual_output.front(), expected_output.front());
                                EXPECT_EQ(actual_output.back(), expected_output.back())
                                    << BATT_INSPECT(input_size) << actual_output.dump();
                            }
                            EXPECT_FALSE(actual_output.empty());
                            ASSERT_THAT(actual_output, ::testing::ElementsAreArray(expected_output));

                            for (usize i = 0; i < expected_output.size(); ++i) {
                                EXPECT_EQ(actual_output[i], expected_output[i]);
                            }
                        }
                    }

                    if (min_delta == 0) {
                        break;
                    }

                    ++seed_delta;
                }  // max_delta
            }      // min_delta
        }          // input_size
    }              // seed
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AlgoParallelRunningTotalTest, CustomValueType)
{
    WorkerPool& worker_pool = WorkerPool::default_pool();

    for (usize loop = 0; loop < 10000; ++loop) {
        std::vector<CustomValue> input;

        for (int i = int(loop); i < int(loop) + 1000; ++i) {
            input.emplace_back(CustomValue{
                .x = i,
                .y = i * 2,
                .z = i * 3,
            });
        }

        batt::BasicRunningTotal<CustomValue> total =
            batt::parallel_running_total(worker_pool, input.begin(), input.end(),
                                         batt::WorkSliceParams{
                                             .min_task_size = batt::TaskSize{10},
                                             .max_tasks = batt::TaskCount{worker_pool.size()},
                                         });

        ASSERT_EQ(total.size(), input.size() + 1);

        int tx = 0;
        int ty = 0;
        int tz = 0;
        for (int i = int(loop); i < int(loop + input.size()) + 1; ++i) {
            EXPECT_EQ(total[i - loop].x, tx) << BATT_INSPECT(i) << BATT_INSPECT(loop);
            EXPECT_EQ(total[i - loop].y, ty) << BATT_INSPECT(i) << BATT_INSPECT(loop);
            EXPECT_EQ(total[i - loop].z, tz) << BATT_INSPECT(i) << BATT_INSPECT(loop);

            if (i < int(loop + input.size())) {
                tx += i;
                ty += i * 2;
                tz += i * 3;
            }
        }

        EXPECT_EQ(total.front(), (CustomValue{0, 0, 0}));
        EXPECT_EQ(total.back(), (CustomValue{tx, ty, tz})) << BATT_INSPECT(loop);
    }

    batt::BasicRunningTotal<CustomValue> empty_total;

    EXPECT_EQ(empty_total.size(), 1u);
    EXPECT_EQ(empty_total.front(), (CustomValue{}));
    EXPECT_EQ(empty_total.front(), (CustomValue{0, 0, 0}));
    EXPECT_EQ(empty_total.back(), (CustomValue{}));
    EXPECT_EQ(empty_total.back(), (CustomValue{0, 0, 0}));
    EXPECT_EQ(empty_total[0], (CustomValue{}));
    EXPECT_EQ(empty_total[0], (CustomValue{0, 0, 0}));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AlgoParallelRunningTotalTest, CustomValueTypeFn)
{
    WorkerPool& worker_pool = WorkerPool::default_pool();

    for (usize loop = 0; loop < 10000; ++loop) {
        std::vector<int> input;

        for (int i = int(loop); i < int(loop) + 1000; ++i) {
            input.emplace_back(i);
        }

        batt::BasicRunningTotal<CustomValue> total = batt::parallel_running_total(
            worker_pool, input.begin(), input.end(),
            [](int i) {
                return CustomValue{
                    .x = i,
                    .y = i * 2,
                    .z = i * 3,
                };
            },
            batt::WorkSliceParams{
                .min_task_size = batt::TaskSize{10},
                .max_tasks = batt::TaskCount{worker_pool.size()},
            });

        ASSERT_EQ(total.size(), input.size() + 1);

        int tx = 0;
        int ty = 0;
        int tz = 0;
        for (int i = int(loop); i < int(loop + input.size()) + 1; ++i) {
            EXPECT_EQ(total[i - loop].x, tx) << BATT_INSPECT(i) << BATT_INSPECT(loop);
            EXPECT_EQ(total[i - loop].y, ty) << BATT_INSPECT(i) << BATT_INSPECT(loop);
            EXPECT_EQ(total[i - loop].z, tz) << BATT_INSPECT(i) << BATT_INSPECT(loop);

            if (i < int(loop + input.size())) {
                tx += i;
                ty += i * 2;
                tz += i * 3;
            }
        }

        EXPECT_EQ(total.front(), (CustomValue{0, 0, 0}));
        EXPECT_EQ(total.back(), (CustomValue{tx, ty, tz})) << BATT_INSPECT(loop);
    }

    batt::BasicRunningTotal<CustomValue> empty_total;

    EXPECT_EQ(empty_total.size(), 1u);
    EXPECT_EQ(empty_total.front(), (CustomValue{}));
    EXPECT_EQ(empty_total.front(), (CustomValue{0, 0, 0}));
    EXPECT_EQ(empty_total.back(), (CustomValue{}));
    EXPECT_EQ(empty_total.back(), (CustomValue{0, 0, 0}));
    EXPECT_EQ(empty_total[0], (CustomValue{}));
    EXPECT_EQ(empty_total[0], (CustomValue{0, 0, 0}));
}

}  // namespace
