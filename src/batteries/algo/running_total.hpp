//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ALGO_RUNNING_TOTAL_HPP
#define BATTERIES_ALGO_RUNNING_TOTAL_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/int_types.hpp>
#include <batteries/interval.hpp>
#include <batteries/slice.hpp>
#include <batteries/stream_util.hpp>
#include <batteries/strong_typedef.hpp>

#include <boost/iterator/iterator_facade.hpp>

#include <functional>
#include <iterator>
#include <memory>
#include <ostream>

namespace batt {

BATT_STRONG_TYPEDEF(usize, PartsCount);
BATT_STRONG_TYPEDEF(usize, PartSize);

template <typename T>
class BasicRunningTotal;

using RunningTotal = BasicRunningTotal<usize>;

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// A parallel-friendly cummulative running total of `usize` values.
//
// This collection is structured as a series of adjacent 'parts' (or shards/partitions) plus a 'summary' that
// records the running totals of the parts.  For example, if the input array (on which we wish to compute the
// running total) is:
//
// `{2, 1, 3, 1, 1, 2, 1, 2, 3, 3, 1, 2}`
//
// We might divide this into three (logical) parts whose running totals can be computed independently in
// parallel:
//
// input parts: `{{2, 1, 3, 1}, {1, 2, 1, 2}, {3, 3, 1, 2}}`
// part totals: `{{2, 3, 6, 7}, {1, 3, 4, 6}, {3, 6, 7, 9}}`
//
// And then the summary is computed as the running total of the individual part totals plus leading zero:
//
// summary: `{0, 7, 13, 22}`
//
// So the overall set of values stored internally by `RunningTotal` for this input would be:
//
// <part-0> + <part-1> + <part-2> + <summary> => `{2, 3, 6, 7, 1, 3, 4, 6, 3, 6, 7, 9, 0, 7, 13, 22}`
//
// This allows a fast O(1) calculation of the overall running total at any point; just add the part-local
// total to the summary for that part.
//
template <typename T>
class BasicRunningTotal
{
   public:
    class Iterator;

    using iterator = Iterator;
    using const_iterator = Iterator;
    using value_type = T;

    using slice_type = boost::iterator_range<Iterator>;

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    BasicRunningTotal() = default;

    BasicRunningTotal(BasicRunningTotal&&) = default;

    explicit BasicRunningTotal(PartsCount count, PartSize size);

    ~BasicRunningTotal() = default;

    BasicRunningTotal& operator=(BasicRunningTotal&&) = default;

    PartsCount parts_count() const
    {
        return PartsCount{this->parts_count_};
    }

    PartSize part_size() const
    {
        return PartSize{this->part_size_};
    }

    usize size() const
    {
        return this->size_;
    }

    bool empty() const
    {
        return this->size() == 0;
    }

    const T& front() const
    {
        BATT_ASSERT(!this->empty());
        return this->values_[0];
    }

    T back() const
    {
        BATT_ASSERT(!this->empty());
        return operator[](this->size() - 1);
    }

    iterator begin() const;

    iterator end() const;

    slice_type slice(usize begin_index, usize end_index) const;

    slice_type slice(Interval<usize> interval) const;

    void reset(PartsCount count, PartSize size);

    void set_size(usize new_size);

    Slice<const T> const_part(usize i) const;

    Slice<T> mutable_part(usize i);

    Slice<const T> const_summary() const;

    Slice<T> mutable_summary();

    T operator[](isize i) const;

    Slice<const T> raw_values() const;

    std::function<void(std::ostream&)> dump() const;

    // Recompute the summary, which is a running total of the parts.  Must be called after parts are
    // updated.
    //
    void update_summary();

   private:
    Slice<const T> const_part_impl(usize i) const;

    Slice<const T> raw_slice(usize offset, usize size) const;

    Slice<T> raw_slice(usize offset, usize size);

    usize raw_size() const;

    usize part_segment_offset(usize part_i) const;

    usize part_segment_size() const;

    usize summary_offset() const;

    usize summary_size() const;

    usize offset_of_part(usize part_i) const;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    std::unique_ptr<T[]> values_{new T[1]{make_default<T>()}};
    PartsCount parts_count_{0};
    PartSize part_size_{1};
    usize size_{1};
};

// #=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

template <typename T>
class BasicRunningTotal<T>::Iterator
    : public boost::iterator_facade<        //
          Iterator,                         // <- Derived
          T,                                // <- Value
          std::random_access_iterator_tag,  // <- CategoryOrTraversal
          T,                                // <- Reference
          isize                             // <- Difference
          >
{
   public:
    using Self = Iterator;
    using iterator_category = std::random_access_iterator_tag;
    using value_type = usize;
    using reference = value_type;

    explicit Iterator(const BasicRunningTotal<T>* container, usize position) noexcept
        : container_{container}
        , position_{static_cast<isize>(position)}
    {
        BATT_ASSERT_GE(this->position_, 0);
    }

    reference dereference() const
    {
        return (*this->container_)[this->position_];
    }

    bool equal(const Self& other) const
    {
        return this->container_ == other.container_ && this->position_ == other.position_;
    }

    void increment()
    {
        BATT_ASSERT_LT(static_cast<usize>(this->position_), this->container_->size());
        ++this->position_;
    }

    void decrement()
    {
        BATT_ASSERT_GT(this->position_, 0);
        --this->position_;
    }

    void advance(isize delta)
    {
        this->position_ += delta;
        BATT_ASSERT_IN_RANGE(0, this->position_, static_cast<isize>(this->container_->size() + 1));
    }

    isize distance_to(const Self& other) const
    {
        return other.position_ - this->position_;
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    const BasicRunningTotal<T>* container() const
    {
        return this->container_;
    }

    isize position() const
    {
        return this->position_;
    }

   private:
    const BasicRunningTotal<T>* container_;
    isize position_;
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

}  // namespace batt

#include <batteries/algo/running_total.ipp>

#endif  // BATTERIES_ALGO_RUNNING_TOTAL_HPP

#include <batteries/config.hpp>
//
#if BATT_HEADER_ONLY
#include <batteries/algo/running_total_impl.hpp>
#endif
