//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_BOOL_STATUS_HPP
#define BATTERIES_BOOL_STATUS_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>

#include <ostream>

namespace batt {

/** \brief An enumeration that models boolean (true/false) values plus unknown.
 */
enum struct BoolStatus : i8 {
    kUnknown = -1,
    kFalse = 0,
    kTrue = 1,
};

/** \brief Constructs a BoolStatus value from a bool.
 */
inline constexpr BoolStatus bool_status_from(bool b) noexcept
{
    if (b) {
        return BoolStatus::kTrue;
    }
    return BoolStatus::kFalse;
}

/** \brief Performs a logical-or (disjunction) between two BoolStatus values.
 *
 * If one of the arguments is BoolStatus::kTrue, then the result is BoolStatus::kTrue.
 * If both arguments are BoolStatus::kFalse, then the result is BoolStatus::kFalse.
 * Otherwise, at least one argument is BoolStatus::kUnknown, and the result is BoolStatus::kUnknown.
 */
inline constexpr BoolStatus operator||(BoolStatus left, BoolStatus right) noexcept
{
    switch (left) {
    case BoolStatus::kFalse:
        return right;

    case BoolStatus::kTrue:
        return BoolStatus::kTrue;

    case BoolStatus::kUnknown:  // fall-through
    default:
        switch (right) {
        case BoolStatus::kTrue:
            return BoolStatus::kTrue;

        case BoolStatus::kFalse:    // fall-through
        case BoolStatus::kUnknown:  // fall-through
        default:
            return BoolStatus::kUnknown;
        }
    }
}

/** \brief Performs a logical-and (conjunction) between two BoolStatus values.
 *
 * If one of the arguments is BoolStatus::kFalse, then the result is BoolStatus::kFalse.
 * If both arguments are BoolStatus::kTrue, then the result is BoolStatus::kTrue.
 * Otherwise, at least one argument is BoolStatus::kUnknown, and the result is BoolStatus::kUnknown.
 */
inline constexpr BoolStatus operator&&(BoolStatus left, BoolStatus right) noexcept
{
    switch (left) {
    case BoolStatus::kTrue:
        return right;

    case BoolStatus::kFalse:
        return BoolStatus::kFalse;

    case BoolStatus::kUnknown:  // fall-through
    default:
        switch (right) {
        case BoolStatus::kFalse:
            return BoolStatus::kFalse;

        case BoolStatus::kTrue:     // fall-through
        case BoolStatus::kUnknown:  // fall-through
        default:
            return BoolStatus::kUnknown;
        }
    }
}

/** \brief Performs a logical-not (negation) for the given BoolStatus value.
 *
 * If b is kUnknown, then the result is also kUnknown.  Otherwise kFalse becomes kTrue and vice versa.
 */
inline constexpr BoolStatus operator!(BoolStatus b) noexcept
{
    switch (b) {
    case BoolStatus::kFalse:
        return BoolStatus::kTrue;

    case BoolStatus::kTrue:
        return BoolStatus::kFalse;

    case BoolStatus::kUnknown:  // fall-through
    default:
        return BoolStatus::kUnknown;
    }
}

/** \brief Prints a BoolStatus value.
 */
inline std::ostream& operator<<(std::ostream& out, const BoolStatus& t) noexcept
{
    switch (t) {
    case BoolStatus::kFalse:
        return out << "False";

    case BoolStatus::kTrue:
        return out << "True";

    case BoolStatus::kUnknown:  // fall-through
    default:
        break;
    }
    return out << "Unknown";
}

}  //namespace batt

#endif  // BATTERIES_BOOL_STATUS_HPP
