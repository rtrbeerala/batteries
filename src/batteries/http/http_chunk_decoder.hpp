//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_CHUNK_DECODER_HPP
#define BATTERIES_HTTP_HTTP_CHUNK_DECODER_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_data.hpp>

#include <batteries/async/buffer_source.hpp>
#include <batteries/async/stream_buffer.hpp>

#include <batteries/pico_http/parser.hpp>

namespace batt {

/** \brief BufferSource transformer that decodes HTTP chunked content fetched from `Src`.
 */
template <typename Src>
class HttpChunkDecoder
{
   public:
    static constexpr usize kBufferVecPreAlloc = 2;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    explicit HttpChunkDecoder(Src&& src,
                              IncludeHttpTrailer consume_trailer = IncludeHttpTrailer{false}) noexcept
        : src_{BATT_FORWARD(src)}
    {
        std::memset(&this->decoder_checkpoint_, 0, sizeof(pico_http::ChunkedDecoder));
        this->decoder_checkpoint_.consume_trailer = consume_trailer;

        this->decoder_latest_ = this->decoder_checkpoint_;
    }

    // The current number of bytes available as consumable data.
    //
    usize size() const
    {
        return this->output_available_ - this->output_consumed_;
    }

    // The decoder has completed.
    //
    bool done() const
    {
        return this->done_;
    }

    // Returns a ConstBufferSequence containing at least `min_count` bytes of data.
    //
    // This method may block the current task if there isn't enough data available to satisfy
    // the request (i.e., if `this->size() < min_count`).
    //
    StatusOr<SmallVec<ConstBuffer, kBufferVecPreAlloc>> fetch_at_least(const i64 min_count_i)
    {
        const usize min_count = BATT_CHECKED_CAST(usize, min_count_i);

        // Keep decoding chunks from the src stream until we have at least the minimum amount of bytes.
        //
        while (this->size() < min_count) {
            if (this->done_) {
                if (this->size() == 0) {
                    this->release_decoded_chunks();
                }
                return {StatusCode::kEndOfStream};
            }

            // Calculate the minimum count we will need to fetch from `src_` to make up the difference between
            // `this->size()` and `min_count`.
            //
            const i64 src_min_count = BATT_CHECKED_CAST(i64, this->decoded_src_size_) +
                                      (min_count_i - BATT_CHECKED_CAST(i64, this->size()));

            // Fetch more source data.
            //
            auto fetched = this->src_.fetch_at_least(src_min_count);

            // Handle the case where the src stream ends before we fetch enough data to satisfy `min_count`.
            //
            if (!fetched.ok() && fetched.status() == StatusCode::kEndOfStream && !this->done_) {
                if (this->size() == 0) {
                    this->release_decoded_chunks();
                }
                return {StatusCode::kClosedBeforeEndOfStream};
            }

            // All other fetch errors are fatal for the decoded stream.
            //
            BATT_REQUIRE_OK(fetched);

            // Rewind the decoder and re-parse the fetched src data.
            //
            this->decoded_src_size_ = 0;
            this->decoded_chunks_.clear();
            this->decoder_latest_ = this->decoder_checkpoint_;

            usize n_to_consume_from_src = 0;

            const auto on_loop_exit = finally([&] {
                if (n_to_consume_from_src > 0) {
                    this->consume_from_src(n_to_consume_from_src);
                }
                this->output_available_ = boost::asio::buffer_size(this->decoded_chunks_);
            });

            for (ConstBuffer src_buffer : *fetched) {
                StatusOr<pico_http::DecodeResult> result =
                    pico_http::decode_chunked(&this->decoder_latest_, src_buffer, &this->decoded_chunks_);

                BATT_REQUIRE_OK(result);

                this->decoded_src_size_ += result->bytes_consumed;

                // On a chunk-by-chunk basis, consume data from src once we have "proven" that the
                // corresponding point in the decoded stream has been consumed by the application.
                //
                if (this->output_consumed_ != 0) {
                    const usize decoded_size = boost::asio::buffer_size(this->decoded_chunks_);
                    if (this->output_consumed_ >= decoded_size) {
                        this->output_consumed_ -= decoded_size;
                        this->decoded_chunks_.clear();
                        this->decoder_checkpoint_ = this->decoder_latest_;

                        // Add all the counts to consume and just make a single call to `this->src_.consume`
                        // at the end of the outer loop.
                        //
                        n_to_consume_from_src += result->bytes_consumed;
                        //
                        // this->decoded_src_size_ will be adjusted later, when we call
                        // `this->consume_from_src(...)`.
                    }
                }

                // Stop decoding chunks once the decoder tells us we've reached end-of-stream.  This only
                // breaks out of the inner loop; we'll go back up to the top and if we have enough decoded
                // data (>= min_count), return successfully, else return StatusCode::kEndOfStream.
                //
                if (result->done) {
                    this->done_ = true;
                    break;
                }
            }
        }

        return consume_buffers_copy(this->decoded_chunks_, this->output_consumed_);
    }

    // Consume the specified number of bytes from the front of the stream so that future calls to
    // `fetch_at_least` will not return the same data.
    //
    void consume(i64 count)
    {
        this->output_consumed_ += count;

        BATT_CHECK_LE(this->output_consumed_, this->output_available_);

        if (this->output_consumed_ == this->output_available_) {
            this->release_decoded_chunks();
        }
    }

    // Unblocks any current and future calls to `prepare_at_least` (and all other fetch/read methods).  This
    // signals to the buffer (and all other clients of this object) that no more data will be read/consumed.
    //
    void close_for_read()
    {
        this->release_decoded_chunks();
    }

   private:
    void release_decoded_chunks()
    {
        this->output_available_ = 0;
        this->output_consumed_ = 0;
        this->decoded_chunks_.clear();
        this->decoder_checkpoint_ = this->decoder_latest_;
        this->consume_from_src(this->decoded_src_size_);
    }

    void consume_from_src(usize count)
    {
        this->decoded_src_size_ -= count;
        this->src_.consume(count);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief The BufferSource containing Transfer-Encoding: chunked data.
     */
    Src src_;

    /** \brief Saves the state of the decoder at the point in the stream where `this->decoded_chunks_` begins.
     * `decoder_latest_` may need to rewind to this point if we fetch more data from `src_`.
     */
    pico_http::ChunkedDecoder decoder_checkpoint_;

    /** \brief The state of the decoder after having decoded the current contents of `this->decoded_chunks_`.
     * This is saved in the object to quickly "fast-forward" when we consume the entire buffered contents of
     * decoded chunks.
     */
    pico_http::ChunkedDecoder decoder_latest_;

    /** \brief Set to true only when the decoder tells us we've read the last chunk.
     */
    bool done_ = false;

    /** \brief The number of bytes from `src_` parsed by the decoder to produce the current
     * `this->decoded_chunks_`.  This should always be at least as large as `this->decoded_chunks_` itself.
     */
    usize decoded_src_size_ = 0;

    /** \brief Cached value of `boost::asio::buffer_size(this->decoded_chunks_)`.
     */
    usize output_available_ = 0;

    /** \brief The number of bytes within `this->decoded_chunks_` that have been consumed via
     * `this->consume(n)`.
     */
    usize output_consumed_ = 0;

    /** \brief The current decoded data chunks.
     */
    SmallVec<ConstBuffer, kBufferVecPreAlloc> decoded_chunks_;
};

static_assert(has_buffer_source_requirements<HttpChunkDecoder<StreamBuffer&>>(), "");

}  // namespace batt

#endif  // BATTERIES_HTTP_HTTP_CHUNK_DECODER_HPP
