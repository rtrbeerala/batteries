// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/operators.hpp>
//
#include <batteries/operators.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
struct Point {
    int x;
    int y;
};

BATT_OBJECT_PRINT_IMPL((), Point, (x, y))
BATT_OBJECT_EQUALS_IMPL((), Point, (x, y))
BATT_OBJECT_LESS_THAN_IMPL((), Point, (x, y))

BATT_EQUALITY_COMPARABLE((), Point, Point)
BATT_TOTALLY_ORDERED((), Point, Point)

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
class PrivatePoint
{
   public:
    friend std::ostream& operator<<(std::ostream&, const PrivatePoint&);
    friend bool operator==(const PrivatePoint&, const PrivatePoint&);
    friend bool operator<(const PrivatePoint&, const PrivatePoint&);

    PrivatePoint(int x, int y) : x_{x}, y_{y}
    {
    }

   private:
    int x_;
    int y_;
};

BATT_OBJECT_PRINT_IMPL((), PrivatePoint, (x_, y_))
BATT_OBJECT_EQUALS_IMPL((), PrivatePoint, (x_, y_))
BATT_OBJECT_LESS_THAN_IMPL((), PrivatePoint, (x_, y_))

BATT_EQUALITY_COMPARABLE((), PrivatePoint, PrivatePoint)
BATT_TOTALLY_ORDERED((), PrivatePoint, PrivatePoint)

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(OperatorsTest, Test)
{
    auto run_tests = [](auto type, const char* expected) {
        using T = typename decltype(type)::type;

        T p{1, 2};

        EXPECT_THAT(batt::to_string(p), expected);

        EXPECT_FALSE(p < p);
        EXPECT_LE(p, p);
        EXPECT_GE(p, p);
        EXPECT_EQ(p, p);

        T q1{0, 2};
        T q2{1, 1};
        T q3{1, 3};
        T q4{2, 1};

        EXPECT_NE(q1, p);
        EXPECT_LT(q1, p);
        EXPECT_LE(q1, p);

        EXPECT_LT(q2, p);
        EXPECT_LE(q2, p);

        EXPECT_GT(q3, p);
        EXPECT_GE(q3, p);

        EXPECT_GT(q4, p);
        EXPECT_GE(q4, p);
    };

    run_tests(batt::StaticType<Point>{}, "Point{ .x=1, .y=2, }");
    run_tests(batt::StaticType<PrivatePoint>{}, "PrivatePoint{ .x_=1, .y_=2, }");
}

}  // namespace
