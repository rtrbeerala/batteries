//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_STACKTRACE_HPP
#define BATTERIES_STACKTRACE_HPP

#ifdef BOOST_STACKTRACE_USE_NOOP
#undef BOOST_STACKTRACE_USE_NOOP
#endif  // BOOST_STACKTRACE_USE_NOOP

// IMPORTANT: This file must not include any batteries/*.hpp!

#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif

#include <boost/stacktrace.hpp>

#if defined(__GNUC__) && !defined(__clang__)
#pragma GCC diagnostic pop
#endif

#endif  // BATTERIES_STACKTRACE_HPP
