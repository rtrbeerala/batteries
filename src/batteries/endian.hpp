//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ENDIAN_HPP
#define BATTERIES_ENDIAN_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>
#include <batteries/utility.hpp>

#include <boost/endian/conversion.hpp>
#include <boost/preprocessor/cat.hpp>

#include <type_traits>

namespace batt {

template <int kBits>
struct ByteSwap;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
// GCC, Clang
//
#if BATT_COMPILER_IS_GCC || BATT_COMPILER_IS_CLANG

template <>
struct ByteSwap<16> {
    BATT_ALWAYS_INLINE u16 operator()(u16 value) const
    {
        return __builtin_bswap16(value);
    }
};

template <>
struct ByteSwap<32> {
    BATT_ALWAYS_INLINE u32 operator()(u32 value) const
    {
        return __builtin_bswap32(value);
    }
};

template <>
struct ByteSwap<64> {
    BATT_ALWAYS_INLINE u64 operator()(u64 value) const
    {
        return __builtin_bswap64(value);
    }
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
// MSVC
//
#elif BATT_COMPILER_IS_MSVC

template <>
struct ByteSwap<16> {
    BATT_ALWAYS_INLINE u16 operator()(u16 value) const
    {
        return _byteswap_ushort(value);
    }
};

template <>
struct ByteSwap<32> {
    BATT_ALWAYS_INLINE u32 operator()(u32 value) const
    {
        return _byteswap_ulong(value);
    }
};

template <>
struct ByteSwap<64> {
    BATT_ALWAYS_INLINE u64 operator()(u64 value) const
    {
        return _byteswap_uint64(value);
    }
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#else
#error Please define ByteSwap for your compiler!
#endif

#define BATT_DEFINE_BIG_ENDIAN_LESS_THAN(bits)                                                               \
    inline bool big_endian_less_than(BOOST_PP_CAT(u, bits) a, BOOST_PP_CAT(u, bits) b)                       \
    {                                                                                                        \
        if (boost::endian::order::little == boost::endian::order::native) {                                  \
            return ByteSwap<bits>{}(a) < ByteSwap<bits>{}(b);                                                \
        }                                                                                                    \
        return a < b;                                                                                        \
    }

BATT_DEFINE_BIG_ENDIAN_LESS_THAN(16)
BATT_DEFINE_BIG_ENDIAN_LESS_THAN(32)
BATT_DEFINE_BIG_ENDIAN_LESS_THAN(64)

#undef BATT_DEFINE_BIG_ENDIAN_LESS_THAN

}  //namespace batt

#endif  // BATTERIES_ENDIAN_HPP
