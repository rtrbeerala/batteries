//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ATOMIC_COUNTER_GAUGE_HPP
#define BATTERIES_ATOMIC_COUNTER_GAUGE_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/int_types.hpp>

namespace batt {

/** \brief An atomic variable that maintains a gauge-like value, which can rise and fall over time, by means
 * of two monotonic counters: one for increases, one for decreases.
 *
 * The monotonic counters that combine to form the gauge value are stored in the upper and lower halves of the
 * bits of a single atomic variable.  Because under this scheme, we must prevent overflow from one counter
 * into the next, each modification to the variable is subsequently normalized by resetting the most
 * significant bit of both the increase counter and decrease counter to zero if they are ever both observed to
 * be 1.  This normalization is done via compare-and-swap loop after an atomic fetch add operation to do
 * either the addition or subtraction.
 */
template <typename IntT>
class AtomicCounterGauge
{
   public:
    using Self = AtomicCounterGauge;

    using value_type = IntT;

    //                      |-------------------------kNumBits--------------------------|
    //                      |--------kNumCountBits--------|--------kNumCountBits--------|
    //                      | |                           | |-----kAddOverflowShift-----|
    //                      | |                           |----------kSubShift----------|
    //                      | |-------------------kSubOverflowShift---------------------|
    //
    // kHalfMask          = |                             |11111111111111111111111111111|
    // kCountMask         = |                             | 1111111111111111111111111111|
    // kGaugeMask         = |                             | 1111111111111111111111111111|
    //
    // kAddIncrement      = |                             |                            1|
    // kAddOverflow       = |                             |10000000000000000000000000000|
    // kAddMask           = |                             |11111111111111111111111111111|
    //
    // kSubIncrement      = |                            1|00000000000000000000000000000|
    // kSubOverflow       = |10000000000000000000000000000|00000000000000000000000000000|
    // kSubMask           = |11111111111111111111111111111|00000000000000000000000000000|
    //

    static constexpr usize kNumBits = sizeof(IntT) * 8;
    static constexpr usize kNumCountBits = kNumBits / 2;

    static_assert(kNumCountBits * 2 == kNumBits);

    static constexpr value_type kHalfMask = ((value_type{1} << kNumCountBits) - 1);
    static constexpr value_type kCountMask = (value_type{1} << (kNumCountBits - 1)) - 1;
    static constexpr value_type kGaugeMask = kCountMask;

    static constexpr usize kAddShift = 0;
    static constexpr usize kAddOverflowShift = kNumCountBits - 1;

    static constexpr usize kSubShift = kNumCountBits;
    static constexpr usize kSubOverflowShift = kNumBits - 1;

    static constexpr value_type kAddIncrement = value_type{1} << kAddShift;
    static constexpr value_type kAddOverflow = value_type{1} << kAddOverflowShift;
    static constexpr value_type kAddMask = kHalfMask << kAddShift;

    static constexpr value_type kSubIncrement = value_type{1} << kSubShift;
    static constexpr value_type kSubOverflow = value_type{1} << kSubOverflowShift;
    static constexpr value_type kSubMask = kHalfMask << kSubShift;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief A value of the atomic variable that implements AtomicCounterGauge.
     *
     * Can be interpreted either as add/sub counters (via State::get_add_count, State::get_sub_count) or as a
     * single combined "gauge" value (via State::get_gauge()).
     *
     * IMPORTANT: the dynamic range of counters and gauges is constrained by the mask values
     * `AtomicCounterGauge::kCountMask` and `AtomicCounterGauge::kGaugeMask` respectively; i.e., if the "true"
     * value of, say, the gauge interpretation of a State `s` is expected to be `x`, then application code may
     * only assume that `(s.get_gauge() & AtomicCounterGauge<T>::kGaugeMask) == (x &
     * AtomicCounterGauge<T>::kGaugeMask)`, but generally NOT that `s.get_gauge() == x`.  This caveat applies
     * to all returned values for this class (so is only explicitly stated here).
     */
    class State
    {
       public:
        friend class AtomicCounterGauge<IntT>;

        //----- --- -- -  -  -   -

        /** \brief Constructs and returns a State value representing the combination of adding and subtracting
         * the given counts.
         */
        static State from_counts(value_type add_count, value_type sub_count) noexcept
        {
            return State{value_type(                       //
                ((add_count & kGaugeMask) << kAddShift) |  //
                ((sub_count & kGaugeMask) << kSubShift)    //
                )};
        }

        /** \brief Constructs and returns a State value representing an instantaneous gauge value.
         *
         * This is equivalent to `State::from_counts(gauge_value, 0)` (or in fact to any expression
         * `State::from_counts(gauge_value + K, K)` for arbitrary K <= AtomicCounterGauge::kGaugeMask).
         */
        static State from_gauge(value_type gauge_value) noexcept
        {
            return State::from_counts(gauge_value, 0);
        }

        /** \brief Returns the normalized form of a given raw state value.
         */
        static value_type normalize(value_type observed_state) noexcept
        {
            // Extract and align the overflow (most-significant) bits of both the add and subtract counters.
            //
            const value_type add_overflow = (observed_state & kAddOverflow) >> kAddShift;
            const value_type sub_overflow = (observed_state & kSubOverflow) >> kSubShift;

            // Iff the overflow bits of add and sub counters are *both* set, then set the common overflow bit.
            //
            const value_type common_overflow = add_overflow & sub_overflow;

            // Subtract any common overflow from both counters.
            //
            const value_type normalized_state =
                observed_state & ~((common_overflow << kAddShift) | (common_overflow << kSubShift));

            return normalized_state;
        }

        //----- --- -- -  -  -   -

        /** \brief Constructs a State from the exact atomic representation `value`.  `value`'s least
         * significant bits should be set to the add counter, and most significant bits should be set to the
         * subtract counter.  The value should also be normalized (i.e., at most 1 of the most significant bit
         * within either count value may be set; if they are both 1, then the normalized form sets them both
         * to 0).
         *
         * NOTE: Because this constructor is used internally in places where it is already known that `value`
         * is in normalized form, this constructor does NOT make any attempt to validate or normalize the
         * passed value.  If you pass a non-normalized value here, behavior is undefined.
         */
        explicit State(value_type value) noexcept : value_{value}
        {
        }

        //----- --- -- -  -  -   -

        /** \brief Returns the gauge interpretation value of the State.
         */
        value_type get_gauge() const noexcept
        {
            return (this->impl_add_count() - this->impl_sub_count()) & kGaugeMask;
        }

        /** \brief Returns the add (positive) component of the counter(s) interpretation of the State.
         */
        value_type get_add_count() const noexcept
        {
            return ((this->value_ >> kAddShift) & kCountMask);
        }

        /** \brief Returns the subtract (negative) component of the counter(s) interpretation of the State.
         */
        value_type get_sub_count() const noexcept
        {
            return ((this->value_ >> kSubShift) & kCountMask);
        }

        /** \brief Returns the raw state value.
         */
        value_type value() const noexcept
        {
            return this->value_;
        }

        //----- --- -- -  -  -   -
       private:
        /** \brief Returns the entire lower half of the state value; this is necessary to accurately compute
         * the gauge value through the entire dynamic range of the variable, since excluding the most
         * significant bit will in some cases break the apparent monotonicity of the counter.
         */
        value_type impl_add_count() const noexcept
        {
            return ((this->value_ >> kAddShift) & kHalfMask);
        }

        /** \brief Returns the entire upper half of the state value; this is necessary to accurately compute
         * the gauge value through the entire dynamic range of the variable, since excluding the most
         * significant bit will in some cases break the apparent monotonicity of the counter.
         */
        value_type impl_sub_count() const noexcept
        {
            return ((this->value_ >> kSubShift) & kHalfMask);
        }

        //----- --- -- -  -  -   -

        /** \brief The state value.
         */
        value_type value_;
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Constructs an AtomicCounterGauge with the given initial (gauge) value.
     */
    explicit AtomicCounterGauge(value_type initial_value = 0) noexcept
        : state_{State::from_gauge(initial_value).value()}
    {
    }

    /** \brief Constructs an AtomicCounterGauge with the given initial raw state value.
     */
    explicit AtomicCounterGauge(State initial_state) noexcept : state_{initial_state.value()}
    {
    }

    /** \brief Atomically loads and returns the value of the variable.
     */
    State load() noexcept
    {
        return State{this->state_.load()};
    }

    /** \brief Atomically updates the (gauge) value of the variable.
     */
    void store(value_type new_value) noexcept
    {
        this->store(State::from_gauge(new_value));
    }

    /** \brief Atomically updates the raw state value of the variable.
     */
    void store(State new_state) noexcept
    {
        this->state_.store(new_state.value());
    }

    /** \brief Atomically swaps the raw state value of the variable, returning its old value.
     */
    State exchange(State new_state) noexcept
    {
        return State{this->state_.exchange(new_state.value())};
    }

    /** \brief Atomically swaps the gauge value of the variable, returning its old value.
     */
    State exchange(value_type new_value) noexcept
    {
        return this->exchange(State::from_gauge(new_value));
    }

    /** \brief Atomically increments the variable by increasing the add counter.
     *
     * `delta` must NOT be negative; use AtomicCounterGauge::fetch_sub in this case.
     */
    State fetch_add(value_type delta) noexcept
    {
        delta = (delta & kCountMask) << kAddShift;

        const value_type old_state = this->state_.fetch_add(delta);
        const value_type new_state = old_state + delta;

        BATT_CHECK_LE((u64)State{old_state}.impl_add_count(),                    //
                      (u64)State{new_state}.impl_add_count())                    //
            << "Add (positive) counter overflow detected! old_state="            //
            << std::bitset<kNumBits>{State{old_state}.value()} << " new_state="  //
            << std::bitset<kNumBits>{State{new_state}.value()};

        this->normalize(new_state);

        return State{old_state};
    }

    /** \brief Same as `this->fetch_add(1)`.
     */
    State increment() noexcept
    {
        return this->fetch_add(1);
    }

    /** \brief Atomically decrements the variable by increasing the sub counter.
     *
     * `delta` must NOT be negative; use AtomicCounterGauge::fetch_sub in this case.
     */
    State fetch_sub(value_type delta) noexcept
    {
        delta = (delta & kCountMask) << kSubShift;

        const value_type old_state = this->state_.fetch_add(delta);
        const value_type new_state = old_state + delta;

        BATT_CHECK_LE((u64)State{old_state}.impl_sub_count(),                    //
                      (u64)State{new_state}.impl_sub_count())                    //
            << "Sub (negative) counter overflow detected! old_state="            //
            << std::bitset<kNumBits>{State{old_state}.value()} << " new_state="  //
            << std::bitset<kNumBits>{State{new_state}.value()};

        this->normalize(new_state);

        return State{old_state};
    }

    /** \brief Same as `this->fetch_sub(1)`.
     */
    State decrement() noexcept
    {
        return this->fetch_sub(1);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    /** \brief Atomically normalizes the variable state using a CAS loop.
     *
     * \see State::normalize.
     */
    value_type normalize(value_type observed_state) noexcept
    {
        for (;;) {
            const value_type normalized_state = State::normalize(observed_state);

            if (BATT_HINT_TRUE(normalized_state == observed_state) ||
                this->state_.compare_exchange_weak(observed_state, normalized_state)) {
                return normalized_state;
            }
        }
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    std::atomic<value_type> state_;
};

}  //namespace batt

#endif  // BATTERIES_ATOMIC_COUNTER_GAUGE_HPP
