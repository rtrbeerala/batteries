#pragma once
#ifndef BATTERIES_SKIP_LIST_HPP
#define BATTERIES_SKIP_LIST_HPP

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>
#include <batteries/math.hpp>
#include <batteries/optional.hpp>

#include <pcg_random.hpp>

#include <array>
#include <bitset>
#include <functional>

namespace batt {

#define BATT_FOR_SKIP_LEVEL_2(start, macro) macro((start)) macro(((start) + 1))

#define BATT_FOR_SKIP_LEVEL_4(start, macro)                                                                  \
    BATT_FOR_SKIP_LEVEL_2(start, macro) BATT_FOR_SKIP_LEVEL_2((start) + 2, macro)

#define BATT_FOR_SKIP_LEVEL_8(start, macro)                                                                  \
    BATT_FOR_SKIP_LEVEL_4((start), macro) BATT_FOR_SKIP_LEVEL_4((start) + 4, macro)

#define BATT_FOR_SKIP_LEVEL_16(start, macro)                                                                 \
    BATT_FOR_SKIP_LEVEL_8((start), macro) BATT_FOR_SKIP_LEVEL_8((start) + 8, macro)

#define BATT_FOR_SKIP_LEVEL_32(start, macro)                                                                 \
    BATT_FOR_SKIP_LEVEL_16((start), macro) BATT_FOR_SKIP_LEVEL_16((start) + 16, macro)

#define BATT_FOR_SKIP_LEVEL_64(start, macro)                                                                 \
    BATT_FOR_SKIP_LEVEL_32((start), macro) BATT_FOR_SKIP_LEVEL_32((start) + 32, macro)

#define BATT_FOR_ALL_SKIP_LEVELS(height_expr, macro)                                                         \
    switch (height_expr) {                                                                                   \
        BATT_FOR_SKIP_LEVEL_64(1, macro)                                                                     \
                                                                                                             \
    default:                                                                                                 \
        BATT_PANIC() << "Bad height value: " << (height_expr);                                               \
        BATT_UNREACHABLE();                                                                                  \
    }

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

template <typename KeyT, typename ValueT, typename OrderFn, i32 kHeight>
class SkipNode;

template <typename KeyT, typename ValueT, typename OrderFn>
class SkipNodeBase
{
   public:
    template <i32 kHeight>
    using Derived = SkipNode<KeyT, ValueT, OrderFn, kHeight>;

    static SkipNodeBase* allocate(i32 new_node_height)
    {
#define BATT_RETURN_NEW_SKIP_NODE(HEIGHT)                                                                    \
    case (HEIGHT):                                                                                           \
        return new Derived<(HEIGHT)>{};
        //----- --- -- -  -  -   -

        BATT_FOR_ALL_SKIP_LEVELS(new_node_height, BATT_RETURN_NEW_SKIP_NODE)
    }

    void deallocate()
    {
#define BATT_DELETE_SKIP_NODE(HEIGHT)                                                                        \
    case (HEIGHT):                                                                                           \
        delete static_cast<Derived<(HEIGHT)>*>(this);                                                        \
        break;
        //----- --- -- -  -  -   -

        BATT_FOR_ALL_SKIP_LEVELS(this->height_, BATT_DELETE_SKIP_NODE)
    }

    SkipNodeBase** get_next(i32 level);

    const SkipNodeBase* const* get_next(i32 level) const;

    Optional<KeyT> key_;
    Optional<ValueT> value_;
    i32 height_;
};

template <typename KeyT, typename ValueT, typename OrderFn, i32 kHeight>
class SkipNode : public SkipNodeBase<KeyT, ValueT, OrderFn>
{
   public:
    using Super = SkipNodeBase<KeyT, ValueT, OrderFn>;

    SkipNode()
    {
        this->height_ = kHeight;
        this->next_.fill(nullptr);
    }

    std::array<Super*, kHeight> next_;
};

template <typename KeyT, typename ValueT, typename OrderFn = std::less<KeyT>, typename Rng = pcg64_unique>
class SkipList
{
   public:
    template <i32 kHeight>
    using Node = SkipNode<KeyT, ValueT, OrderFn, kHeight>;

    using NodeBase = SkipNodeBase<KeyT, ValueT, OrderFn>;

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    SkipList() noexcept : rng_{}, active_levels_{0}, head_{}, size_{0}
    {
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    i32 height() const noexcept
    {
        if (this->active_levels_ == 0) {
            return 0;
        }
        return 64 - __builtin_clzll(this->active_levels_);
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    usize size() const noexcept
    {
        return this->size_;
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    bool empty() const noexcept
    {
        return this->size_ == 0;
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    const NodeBase* find(const KeyT& key) const noexcept
    {
        i32 height = this->height();
        if (height == 0) {
            return nullptr;
        }
        i32 level = height - 1;

        OrderFn key_less_than;

        const NodeBase* prev = &this->head_;
        const NodeBase* next;
        const NodeBase* const* p_next;

        for (;;) {
            p_next = prev->get_next(level);
            next = *p_next;
            if (!next || (next->key_ && key_less_than(key, *next->key_))) {
                if (level == 0) {
                    return prev;
                }
                --level;
            } else {
                prev = next;
            }
        }

        BATT_UNREACHABLE();
    }

    template <typename KeyArg>
    bool erase(const KeyArg& key_arg)
    {
        i32 height = this->height();
        if (height == 0) {
            return false;
        }
        i32 level = height - 1;

        OrderFn key_less_than;

        NodeBase* prev = &this->head_;
        NodeBase* next;
        NodeBase** p_next;
        NodeBase* found = nullptr;

        for (;;) {
            p_next = prev->get_next(level);
            next = *p_next;
            if (next) {
                BATT_CHECK(next->key_);
                if (found || !key_less_than(key_arg, *next->key_)) {
                    if (next == found || (!found && !key_less_than(*next->key_, key_arg))) {
                        //
                        // Keys are equal!

                        if (found) {
                            BATT_CHECK_EQ(found, next) << "Same key found multiple times, different nodes!";
                        } else {
                            found = next;
                        }

                        // Remove the node from this level of the list.
                        //
                        *p_next = *next->get_next(level);

                    } else {
                        prev = next;
                        continue;
                    }
                }
            }
            if (level == 0) {
                break;
            }
            --level;
        }

        if (found) {
            for (i32 i = 0; i < height; ++i) {
                if (!this->head_.next_[i]) {
                    this->active_levels_ &= ~(u64{1} << i);
                }
            }
            found->deallocate();
        }

        return found != nullptr;
    }

    template <typename KeyArg, typename ValueArg>
    NodeBase* emplace(KeyArg&& key_arg, ValueArg&& value_arg)
    {
        i32 new_node_height = __builtin_clzll(this->rng_() | 1ull) + 1;

        NodeBase* node = nullptr;

        i32 this_height = this->height();
        i32 level = this_height - 1;

        OrderFn key_less_than;

        NodeBase* prev = &this->head_;
        NodeBase* next;
        NodeBase** p_next;

        std::array<NodeBase**, 64> p_next_stack;
        for (i32 i = 0; i < new_node_height; ++i) {
            p_next_stack[i] = &this->head_.next_[i];
        }

        while (level >= 0) {
            p_next = prev->get_next(level);
            next = *p_next;
            if (next) {
                BATT_CHECK(next->key_);
                if (!key_less_than(key_arg, *next->key_)) {
                    if (!key_less_than(*next->key_, key_arg)) {
                        //
                        // Keys are equal!

                        node = next;
                        node->value_.emplace(BATT_FORWARD(value_arg));
                        break;

                    } else {
                        prev = next;
                        continue;
                    }
                }
            }
            if (level < new_node_height) {
                p_next_stack[level] = p_next;
            }
            --level;
        }

        if (!node) {
            node = NodeBase::allocate(new_node_height);
            BATT_ASSERT_EQ(node->height_, new_node_height);

            node->key_.emplace(BATT_FORWARD(key_arg));
            node->value_.emplace(BATT_FORWARD(value_arg));

            for (i32 i = 0; i < new_node_height; ++i) {
                BATT_ASSERT_NOT_NULLPTR(p_next_stack[i]);
                *node->get_next(i) = *p_next_stack[i];
                *p_next_stack[i] = node;
            }

            this->active_levels_ |= (u64{1} << new_node_height) - 1;
            this->size_ += 1;
        }

        return node;
    }

    void check_invariants() const
    {
        for (i32 level = 0; level < i32(this->head_.next_.size()); ++level) {
            u64 mask = u64{1} << level;
            NodeBase* node = this->head_.next_[level];

            BATT_CHECK_EQ((node != nullptr), ((this->active_levels_ & mask) != 0))
                << BATT_INSPECT((void*)node) << BATT_INSPECT(std::bitset<16>{this->active_levels_})
                << BATT_INSPECT(std::bitset<16>{mask}) << BATT_INSPECT(level);

            Optional<KeyT> prev_key = this->head_.key_;
            OrderFn key_less_than;
            while (node) {
                BATT_CHECK(node->key_);
                if (prev_key) {
                    BATT_CHECK(key_less_than(*prev_key, *node->key_))
                        << BATT_INSPECT(prev_key) << BATT_INSPECT(node->key_);
                }
                BATT_CHECK_GT(node->height_, level);
                prev_key = node->key_;
                node = *node->get_next(level);
            }
        }
    }

    Rng rng_;
    u64 active_levels_;
    Node<64> head_;
    usize size_;
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

template <typename KeyT, typename ValueT, typename OrderFn>
auto SkipNodeBase<KeyT, ValueT, OrderFn>::get_next(i32 level) -> SkipNodeBase**
{
    auto* node1 = static_cast<SkipNode<KeyT, ValueT, OrderFn, 1>*>(this);
    return &node1->next_[level];
}

template <typename KeyT, typename ValueT, typename OrderFn>
auto SkipNodeBase<KeyT, ValueT, OrderFn>::get_next(i32 level) const -> const SkipNodeBase* const*
{
    auto* node1 = static_cast<const SkipNode<KeyT, ValueT, OrderFn, 1>*>(this);
    return &node1->next_[level];
}

}  //namespace batt

#endif  // BATTERIES_SKIP_LIST_HPP
