#pragma once
#ifndef BATTERIES_ASSERT_IMPL_HPP
#define BATTERIES_ASSERT_IMPL_HPP

#include <atomic>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_NORETURN BATT_INLINE_IMPL void fail_check_exit()
{
    fail_check_exit_entered().store(true);
    BATT_FAIL_CHECK_OUT << std::endl << std::endl;
    std::abort();
    BATT_UNREACHABLE();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::atomic<bool>& fail_check_exit_entered()
{
    static std::atomic<bool> entered_{false};
    return entered_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::atomic<bool>& fail_check_spin_lock()
{
    static std::atomic<bool> spin_lock_{false};
    return spin_lock_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool lock_fail_check_mutex()
{
    std::atomic<bool>& spin_lock = fail_check_spin_lock();
    thread_local bool locked_by_this_thread = false;
    if (!locked_by_this_thread) {
        while (!spin_lock.exchange(true)) {
            continue;
        }
        locked_by_this_thread = true;
        return true;
    }
    return false;
}

}  //namespace batt

#endif  // BATTERIES_ASSERT_IMPL_HPP
