//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_NO_DESTRUCT_HPP
#define BATTERIES_NO_DESTRUCT_HPP

#include <batteries/config.hpp>
//

#include <batteries/type_traits.hpp>

namespace batt {

template <typename T>
class NoDestruct
{
   public:
    explicit NoDestruct() noexcept
    {
        new (&this->storage_) T{};
    }

    template <typename... Args, typename = EnableIfNoShadow<NoDestruct, Args...>>
    explicit NoDestruct(Args&&... args) noexcept
    {
        new (&this->storage_) T(BATT_FORWARD(args)...);
    }

    NoDestruct(const NoDestruct&) = delete;
    NoDestruct& operator=(const NoDestruct&) = delete;

    ~NoDestruct() noexcept
    {
        // Do nothing!
    }

    //----- --- -- -  -  -   -

    T* pointer() noexcept
    {
        return reinterpret_cast<T*>(&this->storage_);
    }

    const T* pointer() const noexcept
    {
        return reinterpret_cast<const T*>(&this->storage_);
    }

    //----- --- -- -  -  -   -

    T& get() noexcept
    {
        return *this->pointer();
    }

    const T& get() const noexcept
    {
        return *this->pointer();
    }

    //----- --- -- -  -  -   -

    T* operator->() noexcept
    {
        return this->pointer();
    }

    const T* operator->() const noexcept
    {
        return this->pointer();
    }

    //----- --- -- -  -  -   -

    T& operator*() noexcept
    {
        return this->get();
    }

    const T& operator*() const noexcept
    {
        return this->get();
    }

   private:
    std::aligned_storage_t<sizeof(T), alignof(T)> storage_;
};

}  // namespace batt

#endif  // BATTERIES_NO_DESTRUCT_HPP
