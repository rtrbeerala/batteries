//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_LOGGING_HPP
#define BATTERIES_LOGGING_HPP

#include <batteries/config.hpp>

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
#if BATT_WITH_GLOG

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_GCC("-Woverloaded-virtual")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Woverloaded-virtual")
//
#include <glog/logging.h>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()
BATT_UNSUPPRESS_IF_GCC()

#define BATT_LOG_FATAL() LOG(FATAL)
#define BATT_LOG_ERROR() LOG(ERROR)
#define BATT_LOG_WARNING() LOG(WARNING)
#define BATT_LOG_INFO() LOG(INFO)
#define BATT_VLOG VLOG
#define BATT_DLOG DLOG
#define BATT_DVLOG DLOG
#define BATT_VLOG_IS_ON(level) VLOG_IS_ON(level)

#else  // ==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

#include <iostream>

#define BATT_LOG_DISABLED(...)                                                                               \
    if (false)                                                                                               \
    std::cerr

#define BATT_LOG_FATAL BATT_LOG_DISABLED
#define BATT_LOG_ERROR BATT_LOG_DISABLED
#define BATT_LOG_WARNING BATT_LOG_DISABLED
#define BATT_LOG_INFO BATT_LOG_DISABLED
#define BATT_VLOG BATT_LOG_DISABLED
#define BATT_DLOG BATT_LOG_DISABLED
#define BATT_DVLOG BATT_LOG_DISABLED
#define BATT_VLOG_IS_ON(level) false

#endif  // BATT_WITH_GLOG

#endif  // BATTERIES_LOGGING_HPP
