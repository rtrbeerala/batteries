//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASIO_ANY_IO_EXECUTOR_HPP
#define BATTERIES_ASIO_ANY_IO_EXECUTOR_HPP

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_GCC("-Woverloaded-virtual")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Woverloaded-virtual")
//
#include <boost/asio/any_io_executor.hpp>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()
BATT_UNSUPPRESS_IF_GCC()

#endif  // BATTERIES_ASIO_ANY_IO_EXECUTOR_HPP
