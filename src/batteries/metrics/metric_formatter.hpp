//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi, Eitan Steiner
//
#pragma once
#ifndef BATTERIES_METRICS_METRIC_FORMATTER_HPP
#define BATTERIES_METRICS_METRIC_FORMATTER_HPP

#include <batteries/config.hpp>
//
#include <batteries/metrics/metric_registry.hpp>

#include <boost/algorithm/string/replace.hpp>
#include <ostream>

namespace batt {

class MetricFormatter
{
   public:
    MetricFormatter(const MetricFormatter&) = delete;
    MetricFormatter& operator=(const MetricFormatter&) = delete;

    virtual ~MetricFormatter() = default;

    // Called once per ostream before any calls to `format_values` for that ostream.
    //
    virtual void initialize(MetricRegistry& src, std::ostream& dst) = 0;

    // Called any number of times to format the current metric values in `src` to `dst`.
    //
    virtual void format_values(MetricRegistry& src, std::ostream& dst) = 0;

    // Called once per ostream after all calls to `format_values` for that ostream.
    //
    virtual void finished(MetricRegistry& src, std::ostream& dst) = 0;

   protected:
    MetricFormatter() = default;

    // Fully qualified metric name with label keys and values, i.e.: name_key1_value1_key2_value2
    // Sanitize the key/value tokens to not contain ',' chars.
    //
    std::string fully_qualified_name(std::string_view name, const MetricLabelSet& labels)
    {
        std::ostringstream fqn;
        fqn << name;

        if (labels.size() > 0) {
            for (const auto& l : labels) {
                std::string key(l.key);
                std::string value(l.value);

                boost::algorithm::replace_all(key, ",", "_");
                boost::algorithm::replace_all(value, ",", "_");

                fqn << "_" << key << "_" << value;
            }
        }
        return fqn.str();
    }
};

}  // namespace batt

#endif  // BATTERIES_METRICS_METRIC_FORMATTER_HPP

#if BATT_HEADER_ONLY
#include <batteries/metrics/metric_formatter_impl.hpp>
#endif  // BATT_HEADER_ONLY
