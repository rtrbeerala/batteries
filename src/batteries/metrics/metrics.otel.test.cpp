//#############################################################################
// Copyright 2023 Eitan Steiner
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_otel_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>
//
#include <batteries/metrics/metric_collectors.hpp>
#include <batteries/metrics/metric_otel_formatter.hpp>
#include <batteries/metrics/metric_registry.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace {

TEST(OpenTelemetryFormatter, BasicTest)
{
    // Initial state:
    //
    batt::MetricRegistry& registry = ::batt::global_metric_registry();
    std::ostringstream oss;
    batt::MetricOpenTelemetryFormatter otel;
    otel.initialize(registry, oss);

    // Check no formatted data:
    //
    otel.format_values(registry, oss);
    EXPECT_TRUE(oss.str().empty());

    // Define metrics:
    //
    const batt::MetricLabel label{batt::Token("job"), batt::Token("batt")};
    batt::CountMetric<int> count(32);
    batt::GaugeMetric<double> gauge;
    gauge.set(3.14);
    batt::HistogramMetric<int> histogram(2, 0, 3);

    // Register metrics:
    //
    registry.add("test", count, batt::MetricLabelSet{label});
    registry.add("test", gauge /* without optional labels */);
    registry.add("test", histogram, batt::MetricLabelSet{label});
    auto on_test_exit = batt::finally([&] {
        registry.remove(count);
        registry.remove(gauge);
        registry.remove(histogram);
    });

    // Check initial formatted data:
    //
    const std::string expected_initial{
R"(test {job="batt"} 32
test_gauge  3.14
test_histogram_bucket_count {job="batt",le="0"} 0
test_histogram_bucket_count {job="batt",le="2"} 0
test_histogram_bucket_count {job="batt",le="3"} 0
test_histogram_bucket_count {job="batt",le="Inf"} 0
test_histogram_bucket_max {job="batt",le="0"} -2147483648
test_histogram_bucket_max {job="batt",le="2"} -2147483648
test_histogram_bucket_max {job="batt",le="3"} -2147483648
test_histogram_bucket_max {job="batt",le="Inf"} -2147483648
test_histogram_bucket_min {job="batt",le="0"} 2147483647
test_histogram_bucket_min {job="batt",le="2"} 2147483647
test_histogram_bucket_min {job="batt",le="3"} 2147483647
test_histogram_bucket_min {job="batt",le="Inf"} 2147483647
test_histogram_bucket_total {job="batt",le="0"} 0
test_histogram_bucket_total {job="batt",le="2"} 0
test_histogram_bucket_total {job="batt",le="3"} 0
test_histogram_bucket_total {job="batt",le="Inf"} 0
)"};

    otel.format_values(registry, oss);
    EXPECT_THAT(oss.str(), ::testing::StrEq(expected_initial));
    oss.str("");

    // Update metrics:
    //
    for (int i = 0; i <= 4; i++) {
        count.add(i);
        gauge.set(i);
        histogram.update(i);
    }

    // Check final formatted data:
    //
    const std::string expected_final{
R"(test {job="batt"} 42
test_gauge  4
test_histogram_bucket_count {job="batt",le="0"} 1
test_histogram_bucket_count {job="batt",le="2"} 2
test_histogram_bucket_count {job="batt",le="3"} 1
test_histogram_bucket_count {job="batt",le="Inf"} 1
test_histogram_bucket_max {job="batt",le="0"} 0
test_histogram_bucket_max {job="batt",le="2"} 2
test_histogram_bucket_max {job="batt",le="3"} 3
test_histogram_bucket_max {job="batt",le="Inf"} 4
test_histogram_bucket_min {job="batt",le="0"} 0
test_histogram_bucket_min {job="batt",le="2"} 1
test_histogram_bucket_min {job="batt",le="3"} 3
test_histogram_bucket_min {job="batt",le="Inf"} 4
test_histogram_bucket_total {job="batt",le="0"} 0
test_histogram_bucket_total {job="batt",le="2"} 3
test_histogram_bucket_total {job="batt",le="3"} 3
test_histogram_bucket_total {job="batt",le="Inf"} 4
)"};

    otel.format_values(registry, oss);
    EXPECT_THAT(oss.str(), ::testing::StrEq(expected_final));
}

}  // namespace
