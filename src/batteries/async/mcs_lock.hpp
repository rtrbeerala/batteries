//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_MCS_LOCK_HPP
#define BATTERIES_ASYNC_MCS_LOCK_HPP

#include <batteries/config.hpp>
//

#include <batteries/async/futex.hpp>
#include <batteries/cpu_align.hpp>
#include <batteries/int_types.hpp>

#include <atomic>
#include <functional>

namespace batt {

template <bool kMetricsEnabled>
class BasicMCSMutex;

using MCSMutex = BasicMCSMutex<false>;

/** \brief A fair mutual exclusion lock based on the MCS lock algorithm.
 *
 */
template <bool kMetricsEnabled = true>
class BasicMCSMutex
{
   public:
    /** \brief Metric counters for measuring behavior of MCSMutex locks.
     *
     * These are only updatd if kMetricsEnabled is true.
     */
    struct Metrics {
        /** \brief The total number of lock acquire operations.
         */
        usize acquire_count = 0;

        /** \brief The number of lock acquires that were able to take the fast path (single atomic exchange
         * operation).
         */
        usize fast_acquire_count = 0;

        /** \brief The number of lock acquires that succeeded without falling back to a Futex wait.
         */
        usize spin_acquire_count = 0;

        /** \brief The number of lock acquires that did at least one Futex wait before succeeding.
         */
        usize wait_acquire_count = 0;

        /** \brief The total number of pre-Futex-wait spin attempts inside acquire.
         */
        usize init_acquire_spin_count = 0;

        /** \brief The total number of post-Futex-wait spin attempts inside acquire.
         */
        usize wait_acquire_spin_count = 0;

        /** \brief The total number of Futex-wait operations during acquire.
         */
        usize wait_acquire_futex_count = 0;

        /** \brief The total number of lock release operations.
         */
        usize release_count = 0;

        /** \brief The number of times release was able to take the fast path (single CAS operation).
         */
        usize fast_release_count = 0;

        /** \brief The number of times release was able to signal the next lock in the queue directly without
         * spinning.
         */
        usize direct_release_count = 0;

        /** \brief The number of times release needed to enter a spin loop to load the next pointer.
         */
        usize spin_release_count = 0;

        /** \brief The total number of spin iterations in release.
         */
        usize wait_next_spin_count = 0;

        /** \brief Sets all counters back to zero.
         */
        void reset() noexcept
        {
            std::memset(this, 0, sizeof(Metrics));
        }

        /** \brief Add the counts in `other` to `this`.
         */
        Metrics& operator+=(const Metrics& other) noexcept
        {
            this->acquire_count += other.acquire_count;
            this->fast_acquire_count += other.fast_acquire_count;
            this->spin_acquire_count += other.spin_acquire_count;
            this->wait_acquire_count += other.wait_acquire_count;
            this->init_acquire_spin_count += other.init_acquire_spin_count;
            this->wait_acquire_futex_count += other.wait_acquire_futex_count;
            this->release_count += other.release_count;
            this->fast_release_count += other.fast_release_count;
            this->direct_release_count += other.direct_release_count;
            this->spin_release_count += other.spin_release_count;
            this->wait_next_spin_count += other.wait_next_spin_count;
            return *this;
        }
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    static Metrics& metrics()
    {
        thread_local Metrics m;
        return m;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    class Lock
    {
       public:
        static constexpr usize kAcquireSpinCyclesInit = 2048;
        static constexpr usize kAcquireSpinCyclesSteady = 8;

        /** \brief Acquires a lock on the passed mutex.
         */
        explicit Lock(BasicMCSMutex& mutex) noexcept : mutex_{mutex}
        {
            this->lock_impl();
        }

        /** \brief Does not acquire the mutex until this->lock() is called.
         */
        explicit Lock(BasicMCSMutex& mutex, std::defer_lock_t) noexcept : mutex_{mutex}
        {
        }

        /** \brief Releases the lock held by the current thread.
         */
        ~Lock() noexcept
        {
            if (this->owns_lock()) {
                this->unlock_impl();
            }
        }

        //----- --- -- -  -  -   -

        /** \brief MCSMutex::Lock is not copy-constructible.
         */
        Lock(const Lock&) = delete;

        /** \brief MCSMutex::Lock is not copy-assignable.
         */
        Lock& operator=(const Lock&) = delete;

        //----- --- -- -  -  -   -

        /** \brief Returns true iff this Lock currently owns the mutex.
         */
        bool owns_lock() const noexcept
        {
            return this->waiting_to_acquire_->load() == 0;
        }

        /** \brief Lock the mutex.
         *
         * Will panic unless `this->owns_lock()` and `this->needs_reset()` are both false.
         */
        void lock() noexcept
        {
            BATT_CHECK_EQ(this->waiting_to_acquire_->load(), 1u)
                << "attempt to lock() while already holding the mutex or after having unlocked it (Locks can "
                   "only be re-acquired after calling reset())";

            this->lock_impl();
        }

        /** \brief Unlock the mutex.
         *
         * Will panic if `this->owns_lock()` is false.
         */
        void unlock() noexcept
        {
            BATT_CHECK_EQ(this->waiting_to_acquire_->load(), 0u)
                << "unlock() called without owning a lock on the mutex!";

            this->waiting_to_acquire_->store(2);
            this->unlock_impl();
        }

        /** \brief Returns true if this->reset() must be called before this->lock() may be called again.
         *
         * In order to make the normal case as fast as possible, a MCRMutex::Lock must be explicitly reset in
         * between `unlock()` and re-`lock()`.
         */
        bool needs_reset() const noexcept
        {
            return this->waiting_to_acquire_->load() == 2;
        }

        /** \brief Resets the internal lock state so that this lock can re-acquire the mutex.
         *
         * Will panic unless `this->needs_reset()` is true.
         */
        void reset() noexcept
        {
            BATT_CHECK_EQ(this->waiting_to_acquire_->load(), 2u)
                << "reset() called while needs_reset() is false";

            this->next_->store(nullptr);
            this->waiting_to_acquire_->store(1);
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -
       private:
        // See https://dl.acm.org/doi/pdf/10.1145/106973.106999
        //
        // "Synchronization without Contention" 1991, ACM
        //  by John M. Mellor-Crummey & Michael L. Scott
        //
        void lock_impl() noexcept
        {
            if (kMetricsEnabled) {
                ++metrics().acquire_count;
            }

            {
                Lock* const prev = this->mutex_.lock_->exchange(this);
                if (prev == nullptr) {
                    // Fast path; we acquired the lock with no contention; return!
                    //
                    if (kMetricsEnabled) {
                        ++metrics().fast_acquire_count;
                    }
                    this->waiting_to_acquire_->store(0);
                    return;
                }
                BATT_CHECK_NE(prev, this) << "recursive call to lock()!";

                // Let the current end-of-queue know about us.
                //
                prev->next_->store(this);
            }

            // The lock is held by some other task; we must spin to acquire.
            //
            for (usize i = 0; i < kAcquireSpinCyclesInit; ++i) {
                if (kMetricsEnabled) {
                    ++metrics().init_acquire_spin_count;
                }
                if (this->waiting_to_acquire_->load() == 0) {
                    if (kMetricsEnabled) {
                        ++metrics().spin_acquire_count;
                    }
                    goto have_lock;
                }
                //----- --- -- -  -  -   -
                spin_yield();
                //----- --- -- -  -  -   -
            }

            // We have exceeded the maximum allowed spin cycles; enter the Futex-wait loop.
            //
            for (;;) {
                if (kMetricsEnabled) {
                    ++metrics().wait_acquire_futex_count;
                }

                // Wait on the Futex.
                //
                const int retval = futex_wait(this->waiting_to_acquire_.get(), /*last_seen=*/1);
                if (retval != 0 && errno != EAGAIN) {
                    BATT_PANIC() << BATT_INSPECT(retval) << BATT_INSPECT(errno)
                                 << " The MAN page says this should not happen!";
                }

                // Now that the futex has been signaled, we spin for the configured limit before doing
                // another Futex wait.
                //
                for (usize i = 0; i < kAcquireSpinCyclesSteady; ++i) {
                    if (kMetricsEnabled) {
                        ++metrics().wait_acquire_spin_count;
                    }
                    if (this->waiting_to_acquire_->load() == 0) {
                        if (kMetricsEnabled) {
                            ++metrics().wait_acquire_count;
                        }
                        goto have_lock;
                    }
                    //----- --- -- -  -  -   -
                    spin_yield();
                    //----- --- -- -  -  -   -
                }
            }

            // Lock acquired!  We are done.
            //
        have_lock:;
        }

        void unlock_impl() noexcept
        {
            if (kMetricsEnabled) {
                ++metrics().release_count;
            }

            Lock* observed_next = this->next_->load();

            //  If our next pointer is null, then assume we are the last in the queue, which means
            //  mutex_.lock_ should still point to us.  CAS to release the lock under this assumption.
            //
            if (observed_next == nullptr) {
                {
                    Lock* presumed_holder = this;
                    do {
                        if (this->mutex_.lock_->compare_exchange_weak(presumed_holder, nullptr)) {
                            if (kMetricsEnabled) {
                                ++metrics().fast_release_count;
                            }
                            return;
                        }
                    } while (presumed_holder == this);
                }

                // We failed to replace ourselves with nullptr at the end of the queue, which means there must
                // be another lock who came afterwards; since we observed our `next_` to be nullptr above, we
                // must spin-wait until our successor runs the `prev->next_.store(this)` statement in the
                // ctor.
                //
                for (;;) {
                    if (kMetricsEnabled) {
                        ++metrics().wait_next_spin_count;
                    }
                    observed_next = this->next_->load();
                    if (observed_next != nullptr) {
                        if (kMetricsEnabled) {
                            ++metrics().spin_release_count;
                        }
                        break;
                    }
                    //----- --- -- -  -  -   -
                    spin_yield();
                    //----- --- -- -  -  -   -
                }
            } else {
                if (kMetricsEnabled) {
                    ++metrics().direct_release_count;
                }
            }

            // Signal to the next-in-queue that the lock is now free!
            //
            //  IMPORTANT: we must not touch `this` from this point on!
            //
            observed_next->waiting_to_acquire_->store(0);
            futex_notify(observed_next->waiting_to_acquire_.get());
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        /** \brief The mutex this object has locked.
         */
        BasicMCSMutex& mutex_;

        /** \brief The next lock holder after us in the queue; this field is updated by the constructor of
         * `next_`, so we might need to spin/wait for it to be set.
         */
        batt::CpuCacheLineIsolated<std::atomic<Lock*>> next_{nullptr};

        /** \brief Used by the previous lock holder to notify us when lock ownership passes to us.
         */
        batt::CpuCacheLineIsolated<std::atomic<u32>> waiting_to_acquire_{1};
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    BasicMCSMutex() = default;

    BasicMCSMutex(const BasicMCSMutex&) = delete;
    BasicMCSMutex& operator=(const BasicMCSMutex&) = delete;

    /** \brief Returns true iff the mutex is currently held by some Lock instance.
     */
    bool is_locked() const noexcept
    {
        return *this->lock_ != nullptr;
    }

   private:
    batt::CpuCacheLineIsolated<std::atomic<Lock*>> lock_{nullptr};
};

}  //namespace batt

#endif  // BATTERIES_ASYNC_MCS_LOCK_HPP
