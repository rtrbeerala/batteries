// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_CONTINUATION_HPP
#define BATTERIES_ASYNC_CONTINUATION_HPP

#include <batteries/config.hpp>
//
#include <batteries/math.hpp>
#include <batteries/static_assert.hpp>
#include <batteries/static_dispatch.hpp>
#include <batteries/strong_typedef.hpp>
#include <batteries/type_erasure.hpp>
#include <batteries/type_traits.hpp>

#if !defined(NDEBUG) && BATT_PLATFORM_IS_LINUX
#define BOOST_USE_VALGRIND 1
#endif

#include <boost/context/continuation.hpp>
#include <boost/context/fixedsize_stack.hpp>
#include <boost/context/pooled_fixedsize_stack.hpp>
#include <boost/context/protected_fixedsize_stack.hpp>

#include <array>

namespace batt {

using Continuation = boost::context::continuation;

class AbstractStackAllocator : public AbstractValue<AbstractStackAllocator>
{
   public:
    AbstractStackAllocator(const AbstractStackAllocator&) = delete;
    AbstractStackAllocator& operator=(const AbstractStackAllocator&) = delete;

    virtual ~AbstractStackAllocator() = default;

    virtual boost::context::stack_context allocate() = 0;

    virtual void deallocate(boost::context::stack_context&) = 0;

   protected:
    AbstractStackAllocator() = default;
};

template <typename T>
class StackAllocatorImpl : public AbstractValueImpl<AbstractStackAllocator, StackAllocatorImpl, T>
{
   public:
    using Super = AbstractValueImpl<AbstractStackAllocator, StackAllocatorImpl, T>;

    template <typename... Args, typename = EnableIfNoShadow<StackAllocatorImpl, Args&&...>>
    explicit StackAllocatorImpl(Args&&... args) : Super(BATT_FORWARD(args)...)
    {
    }

    boost::context::stack_context allocate() override
    {
        return this->Super::obj_.allocate();
    }

    void deallocate(boost::context::stack_context& ctx) override
    {
        return this->Super::obj_.deallocate(ctx);
    }
};

class StackAllocator
{
   public:
    StackAllocator() noexcept : storage_{}
    {
    }

    StackAllocator(const StackAllocator&) = default;
    StackAllocator& operator=(const StackAllocator&) = default;

    template <typename T, typename = EnableIfNoShadow<StackAllocator, T&&>>
    explicit StackAllocator(T&& obj) : storage_{StaticType<std::decay_t<T>>{}, BATT_FORWARD(obj)}
    {
    }

    boost::context::stack_context allocate() const
    {
        return this->storage_->allocate();
    }

    void deallocate(boost::context::stack_context& ctx) const
    {
        return this->storage_->deallocate(ctx);
    }

   private:
    TypeErasedStorage<AbstractStackAllocator, StackAllocatorImpl> storage_;
};

#ifdef BOOST_USE_VALGRIND
BATT_STATIC_ASSERT_EQ(sizeof(void*) * 3, sizeof(boost::context::stack_context));
#else
BATT_STATIC_ASSERT_EQ(sizeof(void*) * 2, sizeof(boost::context::stack_context));
#endif

enum struct StackType {
    kFixedSize = 0,
    kProtectedFixedSize = 1,
    kPooledFixedSize = 2,
    kMaxValue,
};

BATT_STRONG_TYPEDEF(usize, StackSize);

constexpr usize kMinStackSizeLog2 = 10u;
constexpr usize kMaxStackSizeLog2 = 32u;

template <typename T>
inline const StackAllocator& get_stack_allocator_with_type(StackSize stack_size)
{
    static const std::array<StackAllocator, kMaxStackSizeLog2> instance = [] {
        std::array<StackAllocator, kMaxStackSizeLog2> a;
        usize z = 1;
        for (usize i = 0; i < a.size(); ++i) {
            if (i >= kMinStackSizeLog2) {
                BATT_CHECK_EQ(z, usize{1} << i);
                a[i] = StackAllocator{T{usize{1} << i}};
            }
            z *= 2;
        }
        return a;
    }();

    const usize n = std::max<usize>(kMinStackSizeLog2, log2_ceil(stack_size));

    BATT_CHECK_GE(n, kMinStackSizeLog2);
    BATT_CHECK_LT(n, instance.size());
    BATT_CHECK_GE(usize{1} << n, stack_size);

    return instance[n];
}

inline const StackAllocator& get_stack_allocator(StackSize stack_size, StackType stack_type)
{
    switch (stack_type) {
    case StackType::kFixedSize:
        return get_stack_allocator_with_type<boost::context::fixedsize_stack>(stack_size);

    case StackType::kProtectedFixedSize:
        return get_stack_allocator_with_type<boost::context::protected_fixedsize_stack>(stack_size);

    case StackType::kPooledFixedSize:
        BATT_PANIC() << "This stack allocator type is not thread-safe; do not use yet!";
        return get_stack_allocator_with_type<boost::context::pooled_fixedsize_stack>(stack_size);

    case StackType::kMaxValue:  // fall-through
    default:
        break;
    }
    BATT_PANIC() << "Bad stack type: " << static_cast<int>(stack_type);
    BATT_UNREACHABLE();
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

template <typename Fn>
inline Continuation callcc(StackAllocator&& stack_allocator, Fn&& fn)
{
    return boost::context::callcc(std::allocator_arg, std::move(stack_allocator), BATT_FORWARD(fn));
}

template <typename Fn>
inline Continuation callcc(StackSize stack_size, StackType stack_type, Fn&& fn)
{
    return callcc(make_copy(get_stack_allocator(stack_size, stack_type)), BATT_FORWARD(fn));
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_CONTINUATION_HPP
