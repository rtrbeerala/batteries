//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_ASYNC_RUN_TASK_HPP
#define BATTERIES_ASYNC_ASYNC_RUN_TASK_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/preallocated_task.hpp>
#include <batteries/utility.hpp>

namespace batt {

/** \brief Creates and starts (default) a new batt::Task, the memory for which (including stack) is allocated
 * as a single contiguous memory region using the allocated associated with the final arg passed to this
 * function, the completion handler.
 *
 * The completion handler will automatically be called when the task completes; the memory associated with the
 * task will also be automatically freed before (a copy of) the completion handler is invoked.  The completion
 * handler signature is `void()`.
 *
 */
template <typename BodyFn = void(), typename... TaskArgsAndHandler>
Task* async_run_task(const boost::asio::any_io_executor& ex, StackSize stack_byte_size, BodyFn&& body_fn,
                     TaskArgsAndHandler&&... task_args_and_handler)
{
    auto* preallocated_task = rotate_args_right(
        [](auto&& completion_handler, const boost::asio::any_io_executor& ex, StackSize stack_byte_size,
           BodyFn&& body_fn, auto&&... task_args) {
            using CompletionHandlerFn = std::decay_t<decltype(completion_handler)>;

            static_assert(std::is_same_v<decltype(completion_handler), CompletionHandlerFn&&>);

            return PreallocatedTask<CompletionHandlerFn>::make_new(ex, BATT_FORWARD(completion_handler),
                                                                   stack_byte_size, BATT_FORWARD(body_fn),
                                                                   BATT_FORWARD(task_args)...);
        },
        ex, stack_byte_size, BATT_FORWARD(body_fn), BATT_FORWARD(task_args_and_handler)...);

    return preallocated_task->task_pointer();
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_ASYNC_RUN_TASK_HPP
