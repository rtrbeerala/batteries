// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_PREALLOCATED_TASK_HPP
#define BATTERIES_ASYNC_PREALLOCATED_TASK_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/task.hpp>
#include <batteries/int_types.hpp>

#include <boost/asio/associated_allocator.hpp>

#include <memory>
#include <type_traits>
#include <utility>

namespace batt {

template <typename CompletionHandlerFn = void()>
class PreallocatedTask
{
   public:
    using byte_allocator_type = typename std::allocator_traits<
        boost::asio::associated_allocator_t<CompletionHandlerFn>>::template rebind_alloc<char>;

    class PreallocatedStackAllocator
    {
       public:
        PreallocatedStackAllocator() = default;

        explicit PreallocatedStackAllocator(PreallocatedTask* prealloc) noexcept : prealloc_{prealloc}
        {
        }

        PreallocatedStackAllocator(const PreallocatedStackAllocator&) = default;
        PreallocatedStackAllocator& operator=(const PreallocatedStackAllocator&) = default;

        boost::context::stack_context allocate() const
        {
            BATT_CHECK_NOT_NULLPTR(this->prealloc_);

            BATT_CHECK(!this->prealloc_->stack_in_use_);
            this->prealloc_->stack_in_use_ = true;

            return this->prealloc_->get_stack_context();
        }

        void deallocate(boost::context::stack_context& ctx) const
        {
            BATT_CHECK_NOT_NULLPTR(this->prealloc_);

            BATT_CHECK(this->prealloc_->stack_in_use_);
            this->prealloc_->stack_in_use_ = false;

            boost::context::stack_context expected = this->prealloc_->get_stack_context();

            BATT_CHECK_EQ(expected.sp, ctx.sp);
            BATT_CHECK_EQ(expected.size, ctx.size);
        }

       private:
        PreallocatedTask* prealloc_ = nullptr;
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    template <typename BodyFn, typename... TaskArgs>
    static PreallocatedTask* make_new(const boost::asio::any_io_executor& ex,
                                      CompletionHandlerFn&& completion_handler, StackSize stack_byte_size,
                                      BodyFn&& body_fn, TaskArgs&&... task_args)
    {
        byte_allocator_type byte_allocator =
            std::move(boost::asio::get_associated_allocator(completion_handler));

        const usize storage_byte_size = stack_byte_size + sizeof(PreallocatedTask);

        void* storage = byte_allocator.allocate(storage_byte_size);

        return new (storage) PreallocatedTask{ex, BATT_FORWARD(completion_handler), stack_byte_size,
                                              BATT_FORWARD(body_fn), BATT_FORWARD(task_args)...};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    template <typename BodyFn, typename... TaskArgs>
    explicit PreallocatedTask(const boost::asio::any_io_executor& ex,
                              CompletionHandlerFn&& completion_handler, StackSize stack_byte_size,
                              BodyFn&& body_fn, TaskArgs&&... task_args) noexcept
        : completion_handler_{std::move(completion_handler)}
        , stack_in_use_{false}
        , stack_byte_size_{stack_byte_size}
    {
        auto task_options = Task::Options::from_args(BATT_FORWARD(task_args)...);
        Task::DeferStart defer_start = task_options.get_defer_start();
        Optional<Task::GetIsStarted> get_is_started = task_options.get_is_started;

        task_options.get_is_started = None;
        task_options.set_params(Task::DeferStart{true});
        task_options.set_params(StackAllocator{PreallocatedStackAllocator{this}});

        Task* task = new (std::addressof(this->task_))
            Task{/*parent_task_list=*/Task::all_tasks(), ex, BATT_FORWARD(body_fn), std::move(task_options)};

        task->call_when_done([this] {
            BATT_CHECK(!this->stack_in_use_);

            this->task_pointer()->~Task();

            CompletionHandlerFn local_handler = std::move(*this->completion_handler_);
            {
                this->completion_handler_ = None;

                byte_allocator_type byte_allocator =
                    std::move(boost::asio::get_associated_allocator(local_handler));

                const usize storage_byte_size = this->stack_byte_size_ + sizeof(PreallocatedTask);

                this->~PreallocatedTask();

                byte_allocator.deallocate((char*)this, storage_byte_size);
            }
            std::move(local_handler)();
        });

        BATT_CHECK(!task->is_started());

        if (get_is_started && *get_is_started) {
            **get_is_started = !defer_start;
        }

        if (!defer_start) {
            task->start();
        }
    }

    ~PreallocatedTask() noexcept
    {
        BATT_CHECK(!this->stack_in_use_);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    boost::context::stack_context get_stack_context() const noexcept
    {
        u8* const stack_bottom = (u8*)(&this->stack_top_[this->stack_byte_size_]);

        boost::context::stack_context ctx;
        ctx.sp = stack_bottom;
        ctx.size = this->stack_byte_size_;

        return ctx;
    }

    Task* task_pointer() noexcept
    {
        return reinterpret_cast<Task*>(std::addressof(this->task_));
    }

    Task& task_ref() noexcept
    {
        return *this->task_pointer();
    }

    Task* operator->() noexcept
    {
        return this->task_pointer();
    }

    void start() noexcept
    {
        this->task_pointer()->start();
    }

   private:
    Optional<CompletionHandlerFn> completion_handler_;
    bool stack_in_use_;
    usize stack_byte_size_;
    std::aligned_storage_t<sizeof(Task)> task_;
    u8 stack_top_[0];
};

}  //namespace batt

#endif  // BATTERIES_ASYNC_PREALLOCATED_TASK_HPP
