//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/async/inline_sub_task.hpp>
//
#include <batteries/async/inline_sub_task.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/asio/io_context.hpp>
#include <batteries/async/simple_executor.hpp>
#include <batteries/segv.hpp>

#include <thread>
#include <vector>

namespace {

using namespace batt::int_types;
using namespace batt::constants;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AsyncInlineSubTaskTest, Test)
{
    constexpr usize kNumIterations = 10 * 1000;
    constexpr usize kNumPingPongs = 100;
    constexpr usize kTestStackSize = 64 * kKiB;

    for (usize j = 0; j < kNumIterations; ++j) {
        // Run test both with and without deferred start.
        //
        for (bool defer_start : {false, true}) {
            // Make the execution context for both tasks.
            //
            batt::SimpleExecutionContext context;

            std::atomic<bool> completion_handler_called{false};

            // Start a parent task; it will create the child task as an InlineSubTask, reusing part of its
            // stack memory.
            //
            batt::Task parent_task{
                context.get_executor(),
                [&context, defer_start, &completion_handler_called] {
                    batt::Watch<usize> counter{0};

                    std::aligned_storage_t<sizeof(batt::InlineSubTask<kTestStackSize>)> child_task_storage;
                    {
                        batt::Task* const parent_task_ptr = std::addressof(batt::Task::current());
                        BATT_CHECK_NOT_NULLPTR(parent_task_ptr);

                        // Launch the child task; the child will wait for odd values and produce even values.
                        //
                        batt::InlineSubTask<kTestStackSize>& child_task =
                            *(new (std::addressof(child_task_storage)) batt::InlineSubTask<kTestStackSize>{
                                context.get_executor(),
                                [&counter, parent_task_ptr] {
                                    batt::Task* const child_task_ptr = std::addressof(batt::Task::current());

                                    BATT_CHECK_NOT_NULLPTR(child_task_ptr);
                                    BATT_CHECK_NE(child_task_ptr, parent_task_ptr);

                                    for (usize j = 1; j < kNumPingPongs; j += 2) {
                                        EXPECT_TRUE(counter.await_equal(j).ok());
                                        counter.fetch_add(1);
                                    }
                                },
                                batt::Task::DeferStart{defer_start},
                            });

                        auto on_scope_exit = batt::finally([&child_task] {
                            child_task.~InlineSubTask<kTestStackSize>();
                        });

                        // Make sure deferred/immediate start is working correctly.
                        //
                        if (defer_start) {
                            EXPECT_FALSE(child_task.is_started());

                            // Install a completion handler that verifies that it is being run outside of the
                            // child_task inline stack.
                            //
                            child_task.call_when_done([&completion_handler_called] {
                                completion_handler_called.store(true);
                            });

                            //----- --- -- -  -  -   -
                            child_task.start();
                            //----- --- -- -  -  -   -
                            EXPECT_TRUE(child_task.is_started());

                        } else {
                            EXPECT_TRUE(child_task.is_started());
                        }

                        // The parent waits for even values and produces odd values.
                        //
                        for (usize i = 0; i < kNumPingPongs; i += 2) {
                            EXPECT_TRUE(counter.await_equal(i).ok());
                            counter.fetch_add(1);
                        }
                    }

                    // Mess up the memory to make sure there is no low level data race.
                    //
                    std::memset(std::addressof(child_task_storage), 0xba, sizeof(child_task_storage));
                },
            };

            std::vector<std::thread> thread_pool;
            for (usize i = 0; i < 2; ++i) {
                thread_pool.emplace_back([&context] {
                    context.run();
                });
            }

            // Wait for completion.
            //
            for (std::thread& t : thread_pool) {
                t.join();
            }

            if (defer_start) {
                EXPECT_TRUE(completion_handler_called.load());
            } else {
                EXPECT_FALSE(completion_handler_called.load());
            }

            // Tear it all down...
            //
            parent_task.join();
        }
    }
}

}  // namespace
