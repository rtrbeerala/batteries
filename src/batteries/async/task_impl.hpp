//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_TASK_IMPL_HPP
#define BATTERIES_ASYNC_TASK_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/debug_info.hpp>
#include <batteries/async/fake_time_service.hpp>
#include <batteries/async/future.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/logging.hpp>
#include <batteries/no_destruct.hpp>
#include <batteries/stream_util.hpp>

namespace batt {

BATT_INLINE_IMPL i32 next_thread_id()
{
    static std::atomic<i32> id_{1000};
    return id_.fetch_add(1);
}

BATT_INLINE_IMPL i32& this_thread_id()
{
    thread_local i32 id_ = next_thread_id();
    return id_;
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// Task static methods.

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize& Task::nesting_depth()
{
    thread_local usize depth_ = 0;
    return depth_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL i32 Task::next_id()
{
    static std::atomic<i32> id_{1};
    return id_.fetch_add(1);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Task* Task::current_ptr()
{
    return Trampoline::get_current_task();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Task& Task::current()
{
    return *current_ptr();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto Task::all_tasks() -> ConcurrentTaskList&
{
    static NoDestruct<ConcurrentTaskList> instance_;
    return *instance_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::yield()
{
    Task* current_task = Task::current_ptr();
    if (current_task) {
        current_task->yield_impl();
        return;
    }

    std::this_thread::yield();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ Optional<usize> Task::current_stack_pos()
{
    Task* current_task = Task::current_ptr();
    if (current_task) {
        return current_task->stack_pos();
    }
    return None;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ Optional<usize> Task::current_stack_pos_of(const volatile void* ptr)
{
    Task* current_task = Task::current_ptr();
    if (current_task) {
        return current_task->stack_pos_of(ptr);
    }
    return None;
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// Task instance methods.

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Task::~Task() noexcept
{
    BATT_CHECK(!this->scheduler_);
    BATT_CHECK(!this->self_) << BATT_INSPECT(this->id_) << BATT_INSPECT_STR(this->name_)
                             << [this](std::ostream& out) {
                                    print_debug_info(this->debug_info, out);
                                };
    BATT_CHECK(is_terminal_state(this->state_.load())) << "state=" << StateBitset{this->state_.load()};

    this->parent_task_list_.unlink(*this);

    Task::destroy_count().fetch_add(1);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::pre_body_fn_entry(Continuation&& scheduler) noexcept
{
    BATT_VLOG(1) << "Task{.name=" << this->name_ << ",} created on thread " << this_thread_id();

    // Save the base address of the call stack.
    //
    volatile u8 base = 0;
    this->stack_base_ = &base;

    // Transfer control back to the Task ctor.  This Task will be scheduled to run (activated) at the end of
    // the ctor.
    //
    this->suspend_count_ = this->suspend_count_ + 1;
    this->scheduler_ = scheduler.resume();
    this->resume_count_ = this->resume_count_ + 1;

    BATT_VLOG(1) << "Task{.name=" << this->name_ << ",} started on thread " << this_thread_id();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Continuation Task::post_body_fn_exit() noexcept
{
    // Wait for all child tasks to terminate.  IMPORTANT: this needs to happen here, before we return the
    // `scheduler_`/`parent` Continuation, and before we set the kTerminated flag, because the join mechanism
    // relies on Watch::await_equal, which requires this Task to still be in "normal operation" mode.
    //
    this->join_child_tasks();

    Continuation parent = std::move(this->scheduler_);
    BATT_CHECK(parent);

    this->handle_event(kTerminated);

    return parent;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::join_child_tasks()
{
    BATT_DEBUG_INFO(BATT_INSPECT_STR(this->name())
                    << "," << BATT_INSPECT(this->id()) << ": join_child_tasks()"
                    << this->child_tasks_.debug_info(""));

    try {
        this->child_tasks_.await_empty();
    } catch (...) {
        BATT_PANIC() << "this->child_tasks_.await_empty() exited via unhandled exception [task='"
                     << this->name_ << "']: " << boost::current_exception_diagnostic_information();
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::run_completion_handlers()
{
    BATT_CHECK(this->is_done());
    BATT_CHECK(!this->scheduler_);
    BATT_CHECK(!this->self_);

    HandlerList<> local_handlers;
    {
        SpinLockGuard lock{this, kCompletionHandlersLock};
        std::swap(this->completion_handlers_, local_handlers);
        {
            // Set a state bit to make sure that there is no window of time where it is possible to add a
            // new handler after this lambda executes, but before `this->is_done()` returns true.
            //
            // IMPORTANT: This must be the only place we set the kCompletionHandlersClosed bit, AND it must be
            // _after_ we have swapped `this->completion_handlers_` with `local_handlers`!
            //
            const state_type prior_state = this->state_.fetch_or(kCompletionHandlersClosed);
            BATT_CHECK_EQ(prior_state & kCompletionHandlersClosed, state_type(0))
                << BATT_INSPECT(Task::StateBitset{prior_state});
        }
    }

    invoke_all_handlers(&local_handlers);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::wait_for_run_completion_handlers()
{
    // Fast path: since we know that kCompletionHandlersClosed is only set once, while holding the
    // kCompletionHandlersLock (see the BATT_CHECK on `prior_state` in `run_completion_handlers()`), if we see
    // the closed bit set without the lock bit set, then we know we are strictly _after_
    // `run_completion_handlers() releasing its spin lock.  Therefore it is safe to delete the task!
    {
        const state_type observed_state = this->state_.load();
        if ((observed_state & (kCompletionHandlersClosed | kCompletionHandlersLock)) ==
            kCompletionHandlersClosed) {
            return;
        }
    }

    // Acquire a lock on completion handlers to make sure that there isn't a concurrent
    // (racing) thread inside `Task::run_completion_handlers()`.  When `handler` is invoked,
    // we must be _absolutely sure_ that it is safe to delete this Task.
    //
    for (;;) {
        SpinLockGuard lock{this, kCompletionHandlersLock};
        BATT_CHECK(this->is_done());
        BATT_CHECK(this->completion_handlers_.empty());

        // We have to be sure that Task::run_completion_handlers has been invoked and has gotten
        // past the point where it will read anything from `this`.  Because the
        // kCompletionHandlersClosed bit is only set at this point, but prior to releasing the
        // kCompletionHandlersLock spin lock (inside run_completion_handlers), if we observe
        // kCompletionHandlersClosed set while holding the spin lock, we know it is safe to invoke
        // `handler` (which may cause `this` to be deleted).
        //
        if ((lock.prior_state() & kCompletionHandlersClosed) != 0) {
            break;
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ErrorCode Task::sleep_impl(const boost::posix_time::time_duration& duration)
{
    SpinLockGuard lock{this, kSleepTimerLock};

    // The deadline_timer is lazily constructed.
    //
    if (!this->sleep_timer_) {
        // First check to see if this Task's executor is configured to use the FakeTimeService.  If so, do
        // a fake wait instead of a real one.
        //
        boost::asio::execution_context& context = this->ex_.context();
        if (boost::asio::has_service<FakeTimeService>(context)) {
            FakeTimeService& fake_time = boost::asio::use_service<FakeTimeService>(context);
            const FakeTimeService::TimePoint expires_at = fake_time.now() + duration;
            return this->await_impl<ErrorCode>([this, &fake_time, expires_at](auto&& handler) {
                fake_time.async_wait(this->ex_, expires_at, BATT_FORWARD(handler));
            });
        }

        this->sleep_timer_.emplace(this->ex_);
    }

    this->sleep_timer_->expires_from_now(duration);

    return this->await_impl<ErrorCode>([&](auto&& handler) {
        this->sleep_timer_->async_wait(BATT_FORWARD(handler));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize Task::stack_pos() const
{
    volatile u8 pos = 0;
    return this->stack_pos_of(&pos);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize Task::stack_pos_of(const volatile void* ptr) const
{
    const u8* pos = (const u8*)ptr;
    if (pos < this->stack_base_) {
        return this->stack_base_ - pos;
    } else {
        return pos - this->stack_base_;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::join()
{
    BATT_DEBUG_INFO(BATT_INSPECT_STR(this->name()) << "," << BATT_INSPECT(this->id()));

    NoneType ignored = Task::await<NoneType>([this](auto&& handler) {
        this->call_when_done(bind_handler(BATT_FORWARD(handler), [](auto&& handler) {
            BATT_FORWARD(handler)(None);
        }));
    });
    (void)ignored;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Task::IsDone Task::try_join()
{
    return this->is_done();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Task::IsDone Task::is_done() const
{
    return IsDone{Task::is_terminal_state(this->state_.load())};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool Task::wake()
{
    SpinLockGuard lock{this, kSleepTimerLock};

    if (this->sleep_timer_) {
        ErrorCode ec;
        this->sleep_timer_->cancel(ec);
        if (!ec) {
            return true;
        }
    }
    return false;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::handle_event(u32 event_mask)
{
    const u32 new_state = this->state_.fetch_or(event_mask) | event_mask;

    if (is_ready_state(new_state)) {
        const bool force_post = this->is_preempted_;
        this->is_preempted_ = false;
        this->schedule_to_run(new_state, force_post);
        //
    } else if (is_terminal_state(new_state)) {
        // NOTE: we only enter this code path once the context has been swapped back to the Trampoline
        // (thereby deactivating the task stack).  This is crucial to allowing the Task to be deleted from
        // inside a completion handler!
        //
        BATT_CHECK_EQ(Task::current_ptr(), nullptr);
        BATT_CHECK(!this->self_);
        BATT_CHECK(!this->scheduler_);

        BATT_VLOG(1) << "[Task] " << this->name_ << " exiting";

        this->run_completion_handlers();
        //
        // IMPORTANT: there must be no access of `this` after `run_completion_handlers()`, since one of
        // the completion handlers may have deleted the Task object.
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::schedule_to_run(u32 observed_state, bool force_post)
{
    for (;;) {
        if (!is_ready_state(observed_state)) {
            return;
        }
        const u32 target_state = observed_state & ~(kSuspended | kNeedSignal | kHaveSignal);
        if (this->state_.compare_exchange_weak(observed_state, target_state)) {
            break;
        }
    }

    BATT_CHECK(is_ready_state(observed_state));
    BATT_CHECK(this->self_);

    if (!force_post && Task::nesting_depth() < kMaxNestingDepth) {
        ++Task::nesting_depth();
        auto on_scope_exit = batt::finally([] {
            --Task::nesting_depth();
        });
        this->activate_via_dispatch();
    } else {
        this->activate_via_post();
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Task::IsDone Task::run()
{
    // If the sleep timer lock was held *the last time* we yielded control, then re-acquire it now.
    //
    u32 observed_state = this->state_.load();
    if (observed_state & kSleepTimerLockSuspend) {
        for (;;) {
            if (observed_state & kSleepTimerLock) {
                observed_state = this->state_.load();
                continue;
            }
            const u32 target_state = (observed_state & ~kSleepTimerLockSuspend) | kSleepTimerLock;
            if (this->state_.compare_exchange_weak(observed_state, target_state)) {
                break;
            }
        }
    }

    this->resume_impl();

    // If the sleep timer lock was held *this time* when we yielded, then atomically release it and set
    // the kSleepTimerLockSuspend bit so we re-acquire it next time.
    //
    observed_state = this->state_.load();
    if (observed_state & kSleepTimerLock) {
        for (;;) {
            const u32 target_state = (observed_state & ~kSleepTimerLock) | kSleepTimerLockSuspend;
            if (this->state_.compare_exchange_weak(observed_state, target_state)) {
                break;
            }
        }
    }

    return IsDone{(observed_state & kTerminated) == kTerminated};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::resume_impl()
{
    BATT_CHECK(this->self_) << StateBitset{this->state_.load()};

    this->self_ = this->self_.resume();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::yield_impl()
{
    BATT_CHECK(this->scheduler_) << StateBitset{this->state_.load()};

    for (;;) {
        this->suspend_count_ = this->suspend_count_ + 1;
        this->scheduler_ = this->scheduler_.resume();
        this->resume_count_ = this->resume_count_ + 1;

        // If a stack trace has been requested, print it and suspend.
        //
        if (this->state_ & kStackTrace) {
            this->stack_trace_.emplace();
            continue;
        }
        break;
    }

    BATT_CHECK_EQ(Task::current_ptr(), this);
    BATT_CHECK(this->scheduler_) << StateBitset{this->state_.load()};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL u32 Task::spin_lock(u32 lock_mask)
{
    u32 prior_state = 0;

    if (!this->try_spin_lock(lock_mask, prior_state)) {
        for (;;) {
            std::this_thread::yield();
            if (this->try_spin_lock(lock_mask, prior_state)) {
                break;
            }
        }
    }

    return prior_state;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool Task::try_spin_lock(u32 lock_mask, u32& prior_state)
{
    prior_state = this->state_.fetch_or(lock_mask);
    return (prior_state & lock_mask) == 0;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::spin_unlock(u32 lock_mask)
{
    this->state_.fetch_and(~lock_mask);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL i32 Task::backtrace_all(bool force, std::ostream& out)
{
    std::function<i32(TaskList & tasks)> backtrace_all_task_list_fn;
    thread_local i32 nesting_depth = 0;

    backtrace_all_task_list_fn = [&](TaskList& tasks) {
        i32 i = 0;
        for (auto& t : tasks) {
            out << "-- Task{id=" << t.id() << ", name=" << t.name_ << ", suspend=" << t.suspend_count_
                << ", resume=" << t.resume_count_ << "} -------------" << std::endl;
            if (!t.try_dump_stack_trace(force, out)) {
                out << " <no stack available>" << std::endl;
            }
            out << std::endl;
            ++nesting_depth;
            auto on_scope_exit = batt::finally([&] {
                --nesting_depth;
            });
            t.child_tasks_.with_lock(backtrace_all_task_list_fn);
            ++i;
        }
        if (nesting_depth > 1) {
            out << i << " Subtasks are active" << std::endl;
        } else {
            out << i << " Tasks are active" << std::endl;
        }

        print_all_threads_debug_info(out);

        return i;
    };

    return Task::all_tasks().with_lock(backtrace_all_task_list_fn);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool Task::try_dump_stack_trace(bool force, std::ostream& out)
{
    const auto dump_debug_info = [&] {
        if (this->debug_info) {
            out << "DEBUG:" << std::endl;
            print_debug_info(this->debug_info, out);
            out << std::endl;
        }
    };

    u32 observed_state = this->state_.load();

    const auto dump_state_bits = [&](std::ostream& out) {
        if (is_terminal_state(observed_state)) {
            out << "(terminated) ";
        } else if (is_running_state(observed_state)) {
            out << "(running) ";
        } else if (is_ready_state(observed_state)) {
            out << "(ready) ";
        } else if (observed_state & kStackTrace) {
            out << "(busy) ";
        } else {
            out << "(suspended) ";
        }
        out << "state=" << StateBitset{this->state_}
            << " tims,hdlr,timr,dump,term,susp,have,need (0==running)";
    };

    for (;;) {
        if (is_running_state(observed_state) || is_ready_state(observed_state) ||
            is_terminal_state(observed_state) || (observed_state & kStackTrace)) {
            out << dump_state_bits << std::endl;
            if (force) {
                // This is dangerous, but sometimes you just need a clue about what is happening!
                //
                dump_debug_info();
            }
            return false;
        }
        const state_type target_state = observed_state | kStackTrace;
        if (this->state_.compare_exchange_weak(observed_state, target_state)) {
            break;
        }
    }

    out << dump_state_bits << std::endl;

    dump_debug_info();

    this->resume_impl();

    BATT_CHECK(this->stack_trace_);

    out << *this->stack_trace_ << std::endl;
    this->stack_trace_ = None;

    observed_state = this->state_.load();
    for (;;) {
        const state_type target_state = (observed_state & ~kStackTrace) | kSuspended;

        BATT_CHECK(!is_terminal_state(target_state))
            << "This should never happen because we check for terminal state above and calling "
               "Task::resume_impl with the StackTrace bit set should never terminate the task.";

        if (this->state_.compare_exchange_weak(observed_state, target_state)) {
            observed_state = target_state;
            break;
        }
    }
    if (is_ready_state(observed_state)) {
        this->schedule_to_run(observed_state, /*force_post=*/true);
    }
    return true;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto Task::make_activation_handler(bool via_post)
{
    return make_custom_alloc_handler(this->activate_memory_, [this, via_post] {
        if (via_post) {
            BATT_CHECK_EQ(Task::current_ptr(), nullptr);
        }
        Trampoline::activate_task(this);
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::activate_via_post()
{
    boost::asio::post(this->ex_, this->make_activation_handler(/*via_post=*/true));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::activate_via_dispatch()
{
    boost::asio::dispatch(this->ex_, this->make_activation_handler(/*via_post=*/false));
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ void Task::Trampoline::activate_task(Task* task_to_activate) noexcept
{
    auto& self = per_thread_instance();

    while (task_to_activate != nullptr) {
        // If there is a task currently active on the current thread, then we will either soft-preempt it
        // and reschedule the current task, or queue a deferred activation of `task_to_activate` via post.
        //
        if (self.current_task_ != nullptr) {
            if (self.current_task_->get_priority() >= task_to_activate->get_priority()) {
                // In this case, the current task gets to keep running on this thread because it has equal
                // or higher priority.
                //
                task_to_activate->activate_via_post();
            } else {
                // In this case, we're going to force the current task to yield and re-activate it via
                // post. By setting the Trampoline's `next_to_run_` to the task that has preempted it, we
                // arrange for `task_to_activate` to be run after the current task yields.  Because there
                // is a current task and Task::run() may only be called from Trampoline::activate_task,
                // there must be a call to `activate_task` in the current task's scheduling context, so by
                // yielding, we allow that call to run `task_to_activate`.
                //
                BATT_CHECK_EQ(self.next_to_run_, nullptr);
                self.next_to_run_ = task_to_activate;
                self.current_task_->is_preempted_ = true;
                self.current_task_->yield_impl();
            }
            return;  // continue running `current_task`.
        }
        // else (self.current_task_ == nullptr)

        {
            BATT_CHECK_EQ(self.current_task_, nullptr);

            self.current_task_ = task_to_activate;
            auto on_scope_exit = finally([&self, activated_task = task_to_activate] {
                BATT_CHECK_EQ(self.current_task_, activated_task);
                self.current_task_ = nullptr;
                activated_task->handle_event(kSuspended);
                //
                // IMPORTANT: do not touch `activated_task` after handle_event, as the task object may have
                // been deleted!
            });

            task_to_activate->run();
        }

        // Prepare to run the next task.
        //
        task_to_activate = self.next_to_run_;
        self.next_to_run_ = nullptr;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ Task* Task::Trampoline::get_current_task() noexcept
{
    return Task::Trampoline::per_thread_instance().current_task_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ auto Task::Trampoline::per_thread_instance() noexcept -> Trampoline&
{
    thread_local Trampoline instance;
    return instance;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::ConcurrentTaskList::dump_log_info(std::string_view label)
{
    std::unique_lock<std::mutex> lock{this->mutex_};
    Task::ConcurrentTaskList::dump_log_info_locked(lock, label);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::ConcurrentTaskList::dump_log_info_locked(std::unique_lock<std::mutex>& lock,
                                                                     std::string_view label)
{
    BATT_LOG_INFO() << this->debug_info_locked(lock, label);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto Task::ConcurrentTaskList::debug_info(std::string_view label) noexcept
    -> batt::SmallFn<void(std::ostream&)>
{
    return [this, label](std::ostream& out) {
        std::unique_lock<std::mutex> lock{this->mutex_};
        out << this->debug_info_locked(lock, label);
    };
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto Task::ConcurrentTaskList::debug_info_locked(std::unique_lock<std::mutex>&,
                                                                  std::string_view label) noexcept
    -> batt::SmallFn<void(std::ostream&)>
{
    return [this, label](std::ostream& out) {
        out << label << ": (begin)\n";
        for (Task& t : this->task_list_) {
            out << label << ": " << BATT_INSPECT(t.id()) << BATT_INSPECT_STR(t.name()) << "\n";
        }
        out << label << ": (end)";
    };
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::ConcurrentTaskList::push_back(Task& task)
{
    BATT_CHECK(!task.is_linked());

    this->link_count_.fetch_add(1);
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        BATT_ASSERT(!task.is_linked());
        this->task_list_.push_back(task);
        BATT_ASSERT(task.is_linked());
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::ConcurrentTaskList::unlink(Task& task)
{
    if (!task.is_linked()) {
        return;
    }
    {
        std::unique_lock<std::mutex> lock{this->mutex_};
        BATT_ASSERT(task.is_linked());
        task.unlink();
        BATT_ASSERT(!task.is_linked());
    }
    this->unlink_count_.fetch_add(1);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void Task::ConcurrentTaskList::await_empty()
{
    const i64 observed_link_count = this->link_count_.load();
    i64 observed_unlink_count = this->unlink_count_.get_value();

    while (observed_unlink_count != this->link_count_.load()) {
        BATT_CHECK_EQ(observed_link_count, this->link_count_.load())
            << "link_count_ should not change once we enter `await_empty()!";

        BATT_CHECK_LT(observed_unlink_count, observed_link_count)
            << "The link_count and unlink_count should never go backwards, and unlink_count should never get "
               "ahead of the link_count!";

        StatusOr<i64> new_unlink_count = this->unlink_count_.await_not_equal(observed_unlink_count);
        if (!new_unlink_count.ok()) {
            break;
        }
        BATT_CHECK_GE(*new_unlink_count, observed_unlink_count)
            << "unlink_count should never go backwards!" << BATT_INSPECT(this->link_count_.load());

        observed_unlink_count = *new_unlink_count;
    }
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_TASK_IMPL_HPP
