//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_FAKE_TIME_SERVICE_IPP
#define BATTERIES_ASYNC_FAKE_TIME_SERVICE_IPP

#include <batteries/asio/post.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename HandlerFn>
inline void FakeTimeService::State::schedule_timer(FakeTimeService* service_instance,
                                                   const boost::asio::any_io_executor& executor,
                                                   TimePoint expires_at, HandlerFn&& fn)
{
    static_assert(IsCallable<HandlerFn, ErrorCode>{},
                  "async_wait handlers must be callable as: void(boost::system::error_code)");

    // Check to see whether the timer has already expired.
    //
    if (expires_at <= this->now()) {
        boost::asio::post(executor, std::bind(BATT_FORWARD(fn), ErrorCode{}));
        return;
    }

    UniqueHandler<ErrorCode> handler{BATT_FORWARD(fn)};

    TimerInstance timer_instance{
        std::make_shared<TimerInstance::Impl>(service_instance, executor, expires_at, std::move(handler)),
    };

    // Insert the timer into the timer_queue with the lock held.
    {
        Lock lock{this->mutex_};
        this->timer_queue_.push(std::move(timer_instance));
    }
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_FAKE_TIME_SERVICE_IPP
