//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_CANCEL_TOKEN_HPP
#define BATTERIES_ASYNC_CANCEL_TOKEN_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/handler.hpp>
#include <batteries/async/watch.hpp>

#include <batteries/assert.hpp>
#include <batteries/cpu_align.hpp>
#include <batteries/int_types.hpp>
#include <batteries/optional.hpp>
#include <batteries/shared_ptr.hpp>
#include <batteries/small_fn.hpp>
#include <batteries/status.hpp>

#include <bitset>
#include <functional>
#include <ostream>

namespace batt {

// Forward-declarations.
//
class CancelToken;

usize hash_value(const CancelToken& cancel_token);

bool operator==(const CancelToken& l, const CancelToken& r);

bool operator!=(const CancelToken& l, const CancelToken& r);

bool operator<(const CancelToken& l, const CancelToken& r);

bool operator>(const CancelToken& l, const CancelToken& r);

bool operator<=(const CancelToken& l, const CancelToken& r);

bool operator>=(const CancelToken& l, const CancelToken& r);

/** \brief Enables effective cancellation of arbitrary async operations.
 */
class CancelToken
{
   public:
    friend usize hash_value(const CancelToken& cancel_token);
    friend bool operator==(const CancelToken& l, const CancelToken& r);
    friend bool operator<(const CancelToken& l, const CancelToken& r);
    friend bool operator!=(const CancelToken& l, const CancelToken& r);
    friend bool operator>(const CancelToken& l, const CancelToken& r);
    friend bool operator<=(const CancelToken& l, const CancelToken& r);
    friend bool operator>=(const CancelToken& l, const CancelToken& r);

    static constexpr u32 kIdle = 0;
    static constexpr u32 kActiveHandler = 1;
    static constexpr u32 kResolving = 2;
    static constexpr u32 kCompleted = 4;
    static constexpr u32 kCancelled = 8;

    static constexpr bool is_idle_state(u32 value)
    {
        return (value & (kActiveHandler | kResolving | kCompleted)) == 0;
    }

    static constexpr bool is_cancelled_state(u32 value)
    {
        return (value & kCancelled) != 0;
    }

    static constexpr bool is_completed_state(u32 value)
    {
        return (value & kCompleted) != 0;
    }

    static constexpr bool is_resolved_state(u32 value)
    {
        return (value & (kCompleted | kCancelled)) != 0;
    }

    //----- --- -- -  -  -   -

    class Impl : public batt::RefCounted<Impl>
    {
       public:
        bool is_cancelled() const noexcept
        {
            return (this->state_.get_value() & kCancelled) == kCancelled;
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        batt::HandlerMemory<2 * kCpuCacheLineSize - sizeof(batt::Watch<u32>)> memory_;
        batt::Watch<u32> state_{kIdle};
    };

    //----- --- -- -  -  -   -

    template <typename T>
    class HandlerImpl
    {
       public:
        explicit HandlerImpl(batt::SharedPtr<Impl>&& impl, batt::Optional<T>& result) noexcept;

        HandlerImpl(const HandlerImpl&) = default;
        HandlerImpl& operator=(const HandlerImpl&) = default;

        template <typename... Args>
        void operator()(Args&&... args) const noexcept;

        bool is_cancelled() const noexcept
        {
            return this->impl_->is_cancelled();
        }

       private:
        batt::SharedPtr<Impl> impl_;
        batt::Optional<T>* result_;
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    static const CancelToken& none() noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /*implicit*/ CancelToken(NoneType) noexcept;

    CancelToken() = default;

    CancelToken(const CancelToken&) = default;

    CancelToken& operator=(const CancelToken&) = default;

    CancelToken& operator=(const NoneType&) noexcept;

    //----- --- -- -  -  -   -

    template <typename T, typename Fn>
    batt::StatusOr<T> await(Fn&& fn) const noexcept;

    template <typename Fn>
    batt::Status await(Fn&& fn) const noexcept
    {
        return this->await<batt::Status>(BATT_FORWARD(fn));
    }

    void cancel() const noexcept;

    bool is_valid() const noexcept;

    bool is_cancelled() const noexcept;

    explicit operator bool() const noexcept;

    SmallFn<void(std::ostream&)> debug_info() const noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    template <typename T>
    auto make_handler(batt::Optional<T>& result) const noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    batt::SharedPtr<Impl> impl_{new Impl};
};

template <typename T>
inline bool is_cancelled(const CancelToken::HandlerImpl<T>& handler)
{
    return handler.is_cancelled();
}

template <typename T>
inline bool is_cancelled(const CustomAllocHandler<CancelToken::HandlerImpl<T>>& handler)
{
    return is_cancelled(handler.get_handler());
}

/** \brief CancelToken is hashed by its impl pointer.
 */
inline usize hash_value(const CancelToken& cancel_token)
{
    return std::hash<void*>{}(cancel_token.impl_.get());
}

/** \brief Returns true iff the two objects are copies of the same CancelToken.
 */
inline bool operator==(const CancelToken& l, const CancelToken& r)
{
    return l.impl_ == r.impl_;
}

inline bool operator!=(const CancelToken& l, const CancelToken& r)
{
    return !(l == r);
}

inline bool operator<(const CancelToken& l, const CancelToken& r)
{
    return l.impl_ < r.impl_;
}

inline bool operator>(const CancelToken& l, const CancelToken& r)
{
    return r < l;
}

inline bool operator<=(const CancelToken& l, const CancelToken& r)
{
    return !(r < l);
}

inline bool operator>=(const CancelToken& l, const CancelToken& r)
{
    return !(l < r);
}

}  // namespace batt

#include <batteries/async/cancel_token.ipp>

#endif  // BATTERIES_ASYNC_CANCEL_TOKEN_HPP

#include <batteries/config.hpp>
//
#if BATT_HEADER_ONLY
#include <batteries/async/cancel_token_impl.hpp>
#endif
