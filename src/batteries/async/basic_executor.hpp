//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_BASIC_EXECUTOR_HPP
#define BATTERIES_ASYNC_BASIC_EXECUTOR_HPP

#include <batteries/config.hpp>
//

#include <batteries/asio/execution_context.hpp>

#include <type_traits>

namespace batt {

template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
class BasicExecutor
{
   public:
    using Self = BasicExecutor;

    using context_type = ContextT;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr explicit BasicExecutor() noexcept : context_{nullptr}
    {
    }

    constexpr explicit BasicExecutor(ContextT* context) noexcept : context_{context}
    {
        if (std::is_same_v<OutstandingWorkP, boost::asio::execution::outstanding_work_t::tracked_t>) {
            this->on_work_started();
        }
    }

    constexpr BasicExecutor(const Self& other) noexcept : Self{other.context_}
    {
    }

    constexpr BasicExecutor(Self&& other) noexcept : context_{other.context_}
    {
        other.context_ = nullptr;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    Self& operator=(const Self& other) noexcept
    {
        Self tmp{other};
        this->swap(tmp);
        return *this;
    }

    Self& operator=(Self&& other) noexcept
    {
        Self tmp{std::move(other)};
        this->swap(tmp);
        return *this;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    ~BasicExecutor() noexcept
    {
        if (std::is_same_v<OutstandingWorkP, boost::asio::execution::outstanding_work_t::tracked_t>) {
            this->on_work_finished();
        }
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    void swap(Self& other)
    {
        std::swap(this->context_, other.context_);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    ContextT& context() const
    {
        return *this->context_;
    }

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr auto require(boost::asio::execution::blocking_t::possibly_t) const
    {
        return BasicExecutor<ContextT, OutstandingWorkP, boost::asio::execution::blocking_t::possibly_t,
                             RelationshipP>{this->context_};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr auto require(boost::asio::execution::blocking_t::never_t) const
    {
        return BasicExecutor<ContextT, OutstandingWorkP, boost::asio::execution::blocking_t::never_t,
                             RelationshipP>{this->context_};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr auto require(boost::asio::execution::relationship_t::fork_t) const
    {
        return BasicExecutor<ContextT, OutstandingWorkP, BlockingP,
                             boost::asio::execution::relationship_t::fork_t>{this->context_};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr auto require(boost::asio::execution::relationship_t::continuation_t) const
    {
        return BasicExecutor<ContextT, OutstandingWorkP, BlockingP,
                             boost::asio::execution::relationship_t::continuation_t>{this->context_};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr auto require(boost::asio::execution::outstanding_work_t::tracked_t) const
    {
        return BasicExecutor<ContextT, boost::asio::execution::outstanding_work_t::tracked_t, BlockingP,
                             RelationshipP>{this->context_};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr auto require(boost::asio::execution::outstanding_work_t::untracked_t) const
    {
        return BasicExecutor<ContextT, boost::asio::execution::outstanding_work_t::untracked_t, BlockingP,
                             RelationshipP>{this->context_};
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    template <typename OtherAllocator>
    constexpr Self require(boost::asio::execution::allocator_t<OtherAllocator>) const
    {
        return *this;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    constexpr Self require(boost::asio::execution::allocator_t<void>) const
    {
        return *this;
    }

    //----- --- -- -  -  -   -

    template <typename T>
    constexpr auto prefer(boost::asio::execution::prefer_only<T>) const
    {
        return this->require(T{});
    }

    //----- --- -- -  -  -   -

    static constexpr auto query(boost::asio::execution::mapping_t) noexcept  //
        -> boost::asio::execution::mapping_t;

    static constexpr auto query(boost::asio::execution::outstanding_work_t) noexcept  //
        -> boost::asio::execution::outstanding_work_t;

    constexpr auto query(boost::asio::execution::blocking_t) const noexcept  //
        -> boost::asio::execution::blocking_t;

    constexpr auto query(boost::asio::execution::relationship_t) const noexcept  //
        -> boost::asio::execution::relationship_t;

    template <typename OtherAllocator>
    constexpr auto query(boost::asio::execution::allocator_t<OtherAllocator>) const noexcept  //
        -> std::allocator<void>;

    constexpr auto query(boost::asio::execution::allocator_t<void>) const noexcept  //
        -> std::allocator<void>;

    auto query(boost::asio::execution::context_t) const noexcept  //
        -> ContextT&;

    auto query(boost::asio::execution::context_as_t<boost::asio::execution_context&>) const noexcept  //
        -> boost::asio::execution_context&;

    //----- --- -- -  -  -   -

    void on_work_started() const noexcept;

    void on_work_finished() const noexcept;

    template <typename Fn>
    void execute(Fn&& fn) const noexcept;

    template <typename Fn, typename FnAllocator>
    void dispatch(Fn&& fn, FnAllocator&&) const noexcept;

    template <typename Fn, typename FnAllocator>
    void post(Fn&& fn, FnAllocator&&) const noexcept;

    template <typename Fn, typename FnAllocator>
    void defer(Fn&& fn, FnAllocator&&) const noexcept;

   private:
    ContextT* context_;
};

}  //namespace batt

#include <batteries/async/basic_executor.ipp>

#endif  // BATTERIES_ASYNC_BASIC_EXECUTOR_HPP
