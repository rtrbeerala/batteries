//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_DEBUG_INFO_IPP
#define BATTERIES_ASYNC_DEBUG_INFO_IPP

#include <batteries/utility.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename Fn>
inline /*explicit*/ DebugInfoFrame::DebugInfoFrame(Fn&& fn) noexcept : print_info_{BATT_FORWARD(fn)}
{
    this->prev_ = DebugInfoFrame::top();
    DebugInfoFrame::top() = this;
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_DEBUG_INFO_IPP
