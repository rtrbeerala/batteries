//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/slice_work.hpp>
#include <batteries/async/slice_work_impl.hpp>

#endif  // !BATT_HEADER_ONLY
