//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_READ_WRITE_LOCK_HPP
#define BATTERIES_ASYNC_READ_WRITE_LOCK_HPP

#include <batteries/config.hpp>
//

#include <batteries/assert.hpp>
#include <batteries/async/futex.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>
#include <batteries/int_types.hpp>
#include <batteries/logging.hpp>

#include <atomic>
#include <bitset>
#include <ostream>

namespace batt {

struct DumpReadWriteLockState {
    u32 state_value;
};

std::ostream& operator<<(std::ostream& out, const DumpReadWriteLockState& dump);

/** \brief An MCS-style fair reader/writer lock.
 */
template <template <typename> class WatchImplT, bool kPollForNext = false>
class BasicReadWriteLock
{
   public:
    using Lock = BasicReadWriteLock;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    static constexpr u32 kBlockedMask = 0b1;
    static constexpr u32 kBlockedFalse = 0b0;
    static constexpr u32 kBlockedTrue = 0b1;
    static constexpr u32 kSuccessorMask = 0b110;
    static constexpr u32 kSuccessorNone = 0b000;
    static constexpr u32 kSuccessorReader = 0b010;
    static constexpr u32 kSuccessorWriter = 0b100;

    /** \brief Returns a state value representing the combination of the passed bit fields.
     */
    static constexpr u32 make_state(u32 is_blocked, u32 successor_class) noexcept
    {
        return (is_blocked & kBlockedMask) | (successor_class & kSuccessorMask);
    }

    /** \brief Returns true iff the blocked bit of state is set.
     */
    static constexpr bool is_blocked_from_state(u32 state) noexcept
    {
        return (state & kBlockedMask) != 0;
    }

    /** \brief Returns the masked successor class value of `state`, which can be compared to kSuccessorNone,
     * kSuccessorReader, and kSuccessorWriter.
     */
    static constexpr u32 successor_class_from_state(u32 state) noexcept
    {
        return (state & kSuccessorMask);
    }

    using DumpState = DumpReadWriteLockState;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Base class for Reader and Writer scoped lock guards.
     */
    class QueueNode
    {
       public:
        enum NodeClass {
            kReading,
            kWriting,
        };

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        explicit QueueNode(Lock* lock, NodeClass node_class) noexcept : lock_{lock}, class_{node_class}
        {
        }

        //----- --- -- -  -  -   -
        /** \brief The initial fast path is the same for both readers and writers: attempt to fetch-and-store
         * the lock->tail_ pointer to this; if the previous value was nullptr, then `this` is the current
         * holder of the lock (regardless whether it is a writer or a reader).
         */
        QueueNode* fast_lock_or_get_predecessor(u32& observed_state) noexcept
        {
            observed_state = make_state(kBlockedTrue, kSuccessorNone);

            this->next_->set_value(nullptr);
            this->state_->set_value(observed_state);

            return this->lock_->tail_->exchange(this);
        }

        //----- --- -- -  -  -   -
        /** \brief Updates this->state_ (and observed_state), clearing the blocked bit.
         */
        void set_unblocked(u32& observed_state) noexcept
        {
            observed_state = this->state_->fetch_and(~kBlockedTrue) & ~kBlockedTrue;
        }

        //----- --- -- -  -  -   -
        /** \brief Spins/waits for the blocked bit of this->state_ to be 0.
         */
        void await_unblocked(u32& observed_state) noexcept
        {
            // We have exceeded the maximum allowed initial spin cycles; use the more expensive `await_true`
            // method.
            //
            const StatusOr<u32> new_observed_state =  //
                this->state_->await_true([](u32 observed_state) {
                    return !is_blocked_from_state(observed_state);
                });

            BATT_CHECK_OK(new_observed_state);

            observed_state = *new_observed_state;
        }

        //----- --- -- -  -  -   -
        /** \brief Attempts a fast release of the lock.
         *
         * The fast path is the same for both readers and writers; the goal is to *either* replace
         * `lock->tail` as though we were never there (the uncontended case), *or* to obtain the next pointer
         * so we know the successor of this node.  Depending on whether this function is called by a reader or
         * writer, there are slightly different things to do once we know the true value of `next`.
         *
         * \return true iff there is no successor of this (implies the fast unlock succeeded).
         */
        bool fast_unlock(QueueNode*& observed_next) noexcept
        {
            observed_next = this->next_->get_value();

            if (observed_next == nullptr) {
                QueueNode* presumed_tail = this;
                do {
                    if (this->lock_->tail_->compare_exchange_weak(presumed_tail, nullptr)) {
                        // Fast-path release - this QueueNode is (still) the tail and we successfully set
                        // tail to nullptr.  Return!
                        //
                        return true;
                    }
                } while (this == presumed_tail);

                // Fast-path failed, so we know there is a successor to `this`; wait for them to set our
                // `next` pointer.
                //
                this->await_next(observed_next);
            }

            return false;
        }

        //----- --- -- -  -  -   -
        /** \brief Waits until this->next_ is non-null (indicating that the successor of this node is done
         * accessing our state).  Whether or not we will need to use this->next_ depends on the state of
         * `this`, after non-nullptr next is observed.
         */
        void await_next(QueueNode*& observed_next)
        {
            if (kPollForNext) {
                while (observed_next == nullptr) {
                    observed_next = this->next_->poll();
                }

            } else {
                const StatusOr<QueueNode*> new_observed_next = this->next_->await_not_equal(nullptr);
                BATT_CHECK_OK(new_observed_next);
                observed_next = *new_observed_next;
            }
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        Lock* const lock_;
        NodeClass const class_;
        CpuCacheLineIsolated<WatchImplT<QueueNode*>> next_;
        CpuCacheLineIsolated<WatchImplT<u32>> state_;
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    class Reader : private QueueNode
    {
       public:
        explicit Reader(Lock& lock) noexcept : QueueNode{&lock, QueueNode::kReading}
        {
            this->lock_for_read_impl();
        }

        ~Reader() noexcept
        {
            this->unlock_for_read_impl();
        }

        /** \brief Returns the lock for this Reader.
         */
        Lock* lock() const noexcept
        {
            return this->lock_;
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -
       private:
        /** \brief Acquires the lock for reading (exclude all writers, share with readers).
         */
        void lock_for_read_impl() noexcept
        {
            u32 observed_state = 0;

            QueueNode* const pred = this->fast_lock_or_get_predecessor(observed_state);

            if (pred == nullptr) {
                this->lock_->reader_count_->fetch_add(1);
                this->set_unblocked(observed_state);

            } else {
                // If our predecessor is a writer or a blocked reader whose state we were able to update (via
                // CAS) to {successor_class = reader}, then we can count on `pred` to set our state to
                // unblocked, so set pred->next to us and await that state change.
                //
                u32 presumed_pred_state = make_state(kBlockedTrue, kSuccessorNone);

                if (pred->class_ == QueueNode::kWriting ||
                    pred->state_->compare_exchange(presumed_pred_state,
                                                   make_state(kBlockedTrue, kSuccessorReader)))  //
                {
                    pred->next_->set_value(this);
                    this->await_unblocked(observed_state);

                } else {
                    // `pred` is a non-blocked reader; that means we are also unblocked!
                    //
                    this->lock_->reader_count_->fetch_add(1);
                    pred->next_->set_value(this);
                    this->set_unblocked(observed_state);
                }
            }
            //
            // At this point, we are NOT blocked.

            if (successor_class_from_state(observed_state) == kSuccessorReader) {
                QueueNode* observed_next = this->next_->get_value();
                //----- --- -- -  -  -   -
                this->await_next(observed_next);
                //----- --- -- -  -  -   -
                this->lock_->reader_count_->fetch_add(1);
                u32 observed_next_state = 0;
                observed_next->set_unblocked(observed_next_state);
            }
        }

        /** \brief Releases the lock as a reader.
         */
        void unlock_for_read_impl() noexcept
        {
            QueueNode* observed_next = nullptr;

            if (!this->fast_unlock(observed_next)) {
                //
                // At this point, observed_next is NOT nullptr.

                const u32 observed_state = this->state_->get_value();
                if (successor_class_from_state(observed_state) == kSuccessorWriter) {
                    this->lock_->next_writer_->store(observed_next);
                }
            }

            // If this QueueNode is the last reader, then it's our job to wake the next writer in the queue.
            //
            if (this->lock_->reader_count_->fetch_sub(1) == 1) {
                QueueNode* const observed_next_writer = this->lock_->next_writer_->exchange(nullptr);
                if (observed_next_writer != nullptr) {
                    u32 observed_next_writer_state = 0;
                    observed_next_writer->set_unblocked(observed_next_writer_state);
                }
            }
        }
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    class Writer : private QueueNode
    {
       public:
        explicit Writer(Lock& lock) noexcept : QueueNode{&lock, QueueNode::kWriting}
        {
            this->lock_for_write_impl();
        }

        ~Writer() noexcept
        {
            this->unlock_for_write_impl();
        }

        /** \brief Returns the lock for this Reader.
         */
        Lock* lock() const noexcept
        {
            return this->lock_;
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -
       private:
        /** \brief Acquires the lock for writing (total mutual exclusion).
         */
        void lock_for_write_impl() noexcept
        {
            u32 observed_state = 0;

            QueueNode* const pred = this->fast_lock_or_get_predecessor(observed_state);

            if (pred == nullptr) {
                this->lock_->next_writer_->store(this);
                if (this->lock_->reader_count_->load() == 0 &&
                    this->lock_->next_writer_->exchange(nullptr) == this) {
                    this->set_unblocked(observed_state);
                    return;
                }

            } else {
                // IMPORTANT: this assumes the bits 0b110 are zero!
                //
                const u32 prior_pred_state = pred->state_->fetch_or(kSuccessorWriter);
                BATT_CHECK_EQ((prior_pred_state & kSuccessorMask), kSuccessorNone);

                // IMPORTANT: updating pred->next_ must come after modifying pred->state_!
                //
                pred->next_->set_value(this);
            }

            this->await_unblocked(observed_state);
        }

        /** \brief Releases the lock as a writer.
         */
        void unlock_for_write_impl() noexcept
        {
            QueueNode* observed_next = nullptr;

            if (!this->fast_unlock(observed_next)) {
                //
                // At this point, observed_next is NOT nullptr.

                if (observed_next->class_ == QueueNode::kReading) {
                    this->lock_->reader_count_->fetch_add(1);
                }

                // Set next->blocked to false to allow the next thread to run.
                //
                u32 observed_next_state = 0;
                observed_next->set_unblocked(observed_next_state);
            }
        }
    };

    /** \brief Returns the current number of readers holding this lock.
     */
    i64 reader_count() const noexcept
    {
        return this->reader_count_->load();
    }

   private:
    CpuCacheLineIsolated<std::atomic<QueueNode*>> tail_{nullptr};
    CpuCacheLineIsolated<std::atomic<i64>> reader_count_{0};
    CpuCacheLineIsolated<std::atomic<QueueNode*>> next_writer_{nullptr};
};

using ReadWriteLock = BasicReadWriteLock</*WatchImplT=*/WatchAtomic>;

inline std::ostream& operator<<(std::ostream& out, const DumpReadWriteLockState& dump)
{
    out << std::bitset<3>{dump.state_value} << "(";

    if ((dump.state_value & ReadWriteLock::kBlockedMask) == ReadWriteLock::kBlockedTrue) {
        out << "blocked";
    } else {
        out << "unblocked";
    }

    out << ",succ=";

    switch (dump.state_value & ReadWriteLock::kSuccessorMask) {
    case ReadWriteLock::kSuccessorNone:
        out << "none";
        break;

    case ReadWriteLock::kSuccessorReader:
        out << "reader";
        break;

    case ReadWriteLock::kSuccessorWriter:
        out << "writer";
        break;

    default:
        out << "INVALID";
        break;
    }

    return out << ")";
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_READ_WRITE_LOCK_HPP
