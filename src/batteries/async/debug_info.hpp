//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2022 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/async/debug_info_decl.hpp>

#include <batteries/async/debug_info.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/debug_info_impl.hpp>
#endif
