//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_BASIC_EXECUTOR_IPP
#define BATTERIES_ASYNC_BASIC_EXECUTOR_IPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
/*static*/ constexpr auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::mapping_t) noexcept  //
    -> boost::asio::execution::mapping_t
{
    return boost::asio::execution::mapping.thread;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
/*static*/ constexpr auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::outstanding_work_t) noexcept  //
    -> boost::asio::execution::outstanding_work_t
{
    return (std::is_same_v<OutstandingWorkP, boost::asio::execution::outstanding_work_t::tracked_t>)
               ? boost::asio::execution::outstanding_work_t(boost::asio::execution::outstanding_work.tracked)
               : boost::asio::execution::outstanding_work_t(
                     boost::asio::execution::outstanding_work.untracked);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline constexpr auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::blocking_t) const noexcept  //
    -> boost::asio::execution::blocking_t
{
    return (std::is_same_v<BlockingP, boost::asio::execution::blocking_t::never_t>)
               ? boost::asio::execution::blocking_t(  //
                     boost::asio::execution::blocking.never)
               : boost::asio::execution::blocking_t(  //
                     boost::asio::execution::blocking.possibly);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
constexpr auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::relationship_t) const noexcept  //
    -> boost::asio::execution::relationship_t
{
    return (std::is_same_v<RelationshipP, boost::asio::execution::relationship_t::fork_t>)
               ? boost::asio::execution::relationship_t(  //
                     boost::asio::execution::relationship.fork)
               : boost::asio::execution::relationship_t(  //
                     boost::asio::execution::relationship.continuation);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::context_t) const noexcept  //
    -> ContextT&
{
    return *this->context_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::context_as_t<boost::asio::execution_context&>) const noexcept  //
    -> boost::asio::execution_context&
{
    return *this->context_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::on_work_started()
    const noexcept  //
    -> void
{
    if (this->context_ != nullptr) {
        this->context_->on_work_started();
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::on_work_finished()
    const noexcept  //
    -> void
{
    if (this->context_ != nullptr) {
        this->context_->on_work_finished();
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
template <typename Fn>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::execute(
    Fn&& fn) const noexcept  //
    -> void
{
    if (std::is_same_v<BlockingP, boost::asio::execution::blocking_t::possibly_t>) {
        this->context_->dispatch(BATT_FORWARD(fn));
    } else {
        this->context_->post(BATT_FORWARD(fn));
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
template <typename Fn, typename FnAllocator>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::dispatch(
    Fn&& fn, FnAllocator&&) const noexcept  //
    -> void
{
    this->context_->dispatch(BATT_FORWARD(fn));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
template <typename Fn, typename FnAllocator>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::post(
    Fn&& fn, FnAllocator&&) const noexcept  //
    -> void
{
    this->context_->post(BATT_FORWARD(fn));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
template <typename Fn, typename FnAllocator>
inline auto BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::defer(
    Fn&& fn, FnAllocator&&) const noexcept  //
    -> void
{
    this->context_->defer(BATT_FORWARD(fn));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
template <typename OtherAllocator>
inline constexpr std::allocator<void>
BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::allocator_t<OtherAllocator>) const noexcept
{
    return this->context_->allocator_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline constexpr std::allocator<void>
BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>::query(
    boost::asio::execution::allocator_t<void>) const noexcept
{
    return this->context_->allocator_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline constexpr bool operator==(
    const BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>& l,
    const BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>& r) noexcept
{
    return &(l.context()) == &(r.context());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename ContextT, typename OutstandingWorkP, typename BlockingP, typename RelationshipP>
inline constexpr bool operator!=(
    const BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>& l,
    const BasicExecutor<ContextT, OutstandingWorkP, BlockingP, RelationshipP>& r) noexcept
{
    return !(l == r);
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_BASIC_EXECUTOR_IPP
